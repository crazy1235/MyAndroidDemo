package com.jacksen.demo.view.dispatch;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import java.util.List;

/**
 * Created by Admin on 2017/5/4.
 */

public class SlideConflictAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int NORMAL_TYPE = 0;
    private static final int IMAGE_TYPE = 1;

    private Context context;
    private List<String> list;

    public SlideConflictAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == NORMAL_TYPE) {
            return new NormalViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_textview, parent, false));
        } else if (viewType == IMAGE_TYPE) {
            return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slide_conflict_image, parent, false));
        }
        return new NormalViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_textview, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NormalViewHolder) {
//            ((NormalViewHolder) holder).textView.setText("111");
        } else if (holder instanceof ImageViewHolder) {
            SlideConflictImageAdapter adapter = new SlideConflictImageAdapter();
            ((ImageViewHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            ((ImageViewHolder) holder).recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 5) {
            return IMAGE_TYPE;
        }
        return NORMAL_TYPE;
    }

    class NormalViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public NormalViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text_view);
        }
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view);
        }
    }

}
