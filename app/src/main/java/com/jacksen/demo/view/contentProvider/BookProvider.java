package com.jacksen.demo.view.contentProvider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Admin on 2017/5/19.
 */

public class BookProvider extends ContentProvider {


    public static final String AUTHORITY = "com.jacksen.demo.provider.BookProvider";

    public static final Uri BOOK_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/book");

    public static final Uri USER_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/user");

    public static final int BOOK_URI_CODE = 0;

    public static final int USER_URI_CODE = 1;


    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, "book", BOOK_URI_CODE);
        URI_MATCHER.addURI(AUTHORITY, "user", USER_URI_CODE);
    }

    private SQLiteDatabase sqLiteDatabase;

    private String getTableName(Uri uri) {
        String tableName = null;
        switch (URI_MATCHER.match(uri)) {
            case BOOK_URI_CODE:
                tableName = DbOpenHelper.BOOK_TABLE_NAME;
                break;
            case USER_URI_CODE:
                tableName = DbOpenHelper.USER_TABLE_NAME;
                break;
        }
        return tableName;
    }

    private void initProviderData() {
        sqLiteDatabase = new DbOpenHelper(getContext()).getWritableDatabase();
        sqLiteDatabase.execSQL("delete from " + DbOpenHelper.BOOK_TABLE_NAME);
        sqLiteDatabase.execSQL("delete from " + DbOpenHelper.USER_TABLE_NAME);
        sqLiteDatabase.execSQL("insert into book values(1, 'java');");
        sqLiteDatabase.execSQL("insert into book values(2, 'javascript');");
        sqLiteDatabase.execSQL("insert into book values(3, 'html');");
        sqLiteDatabase.execSQL("insert into book values(4, 'SQL');");
        sqLiteDatabase.execSQL("insert into book values(5, 'android');");
        sqLiteDatabase.execSQL("insert into book values(6, 'ios');");
    }

    @Override
    public boolean onCreate() {

        Log.d("BookProvider", "onCreate(), current thread : " + Thread.currentThread().getName());

        initProviderData();

        return true;
    }

    private String getTable(Uri uri) {
        String table = getTableName(uri);
        if (table == null) {
            throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        return table;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Log.d("BookProvider", "query, current thread : " + Thread.currentThread().getName());

        return sqLiteDatabase.query(getTable(uri), projection, selection, selectionArgs, null, null, sortOrder, null);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        Log.d("BookProvider", "getType , current thread : " + Thread.currentThread().getName());
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Log.d("BookProvider", "insert, current thread : " + Thread.currentThread().getName());

        sqLiteDatabase.insert(getTableName(uri), null, values);
        getContext().getContentResolver().notifyChange(uri, null);

        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d("BookProvider", "delete, current thread : " + Thread.currentThread().getName());

        int count = sqLiteDatabase.delete(getTableName(uri), selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d("BookProvider", "update, current thread : " + Thread.currentThread().getName());

        int row = sqLiteDatabase.update(getTableName(uri), values, selection, selectionArgs);
        if (row > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return row;
    }
}
