package com.jacksen.demo.view.time;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import com.jacksen.demo.view.MainActivity;
import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * test chronometer
 */
public class TestChronometer extends AppCompatActivity {

    @Bind(R.id.chronometer)
    Chronometer chronometer;
    @Bind(R.id.start_btn)
    Button startBtn;
    @Bind(R.id.stop_btn)
    Button stopBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_chronometer);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
//                Toast.makeText(TestChronometer.this, chronometer.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.start_btn)
    void startTiming() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
        startActivity(new Intent(TestChronometer.this, MainActivity.class));
    }

    @OnClick(R.id.stop_btn)
    void stopTiming(){
        chronometer.stop();
        Toast.makeText(this, chronometer.getText().toString(), Toast.LENGTH_SHORT).show();
    }
}
