package com.jacksen.demo.view.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ys on 2015/9/11.
 */
public class TestService extends Service {

    private static final String TAG = TestService.class.getSimpleName();

    private MyHandler handler = new MyHandler();

    private Timer timer = new Timer();

    private TimerTask timerTask;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate -- " + this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind -- " + this + "\n" + intent.toString());
       /* timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "beat");
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 5 * 1000);*/
//        handler.sendEmptyMessage(1);
        /*Log.d(TAG, "123");
        handler.post(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.d(TAG, "123");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });*/

        return new MyBinder();
    }

    /**
     *
     */
    private class MyBinder extends Binder {

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand -- " + this + "\n" + intent.toString());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d(TAG, "onRebind -- " + this + "\n" + intent.toString());
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind -- " + this);
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy -- " + this);
        /*timerTask.cancel();
        timer.cancel();*/

    }


    /**
     *
     */
    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:

                    break;
                default:
                    break;
            }
        }
    }

}
