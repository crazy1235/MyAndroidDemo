package com.jacksen.demo.view.annotation;

/**
 * Created by jacksen on 2016/3/8.
 */
public @interface MyAnno {
    String abc();

    int efg();

    double hhh() default 1.0d;
}
