package com.jacksen.demo.view.spinner;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.jacksen.demo.view.R;

public class TestSpinnerActivity extends AppCompatActivity {

    private Spinner spinner;

    private Spinner dateSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_spinner);


        findViewById(R.id.click_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((ArrayAdapter)spinner.getAdapter()).setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        dateSpinner = (Spinner) findViewById(R.id.date_spinner);

        final String[] dates = getResources().getStringArray(R.array.date_spinner_items);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(TestSpinnerActivity.this, dates[position], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, dates) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
//                TextView textView = (TextView) view.findViewById(android.R.id.text1);
//                textView.setGravity(Gravity.CENTER);
//                ((TextView) view).setGravity(Gravity.CENTER);

                return view;
            }
        };*/
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item, android.R.id.text1, dates);
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_item2, dates);
        arrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);


        ////

//        NiceSpinner niceSpinner = (NiceSpinner) findViewById(R.id.nice_spinner);
//        List<String> dataSet = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataSet);

    }
}
