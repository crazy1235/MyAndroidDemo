package com.jacksen.demo.view.numberFormat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Test NumberFormat
 * 格式化
 *
 * @author ys
 */
public class TestNumberFormat extends AppCompatActivity {

    @Bind(R.id.test_tv1)
    TextView testTv1;
    @Bind(R.id.test_tv2)
    TextView testTv2;
    @Bind(R.id.test_tv3)
    TextView testTv3;
    @Bind(R.id.test_tv4)
    TextView testTv4;
    @Bind(R.id.test_tv5)
    TextView testTv5;
    @Bind(R.id.test_tv6)
    TextView testTv6;
    @Bind(R.id.test_tv7)
    TextView testTv7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_number_format);
        ButterKnife.bind(this);

        init();
    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestNumberFormat.class));
    }

    /**
     *
     */
    private void init() {
        NumberFormat mf = NumberFormat.getInstance();
        mf.setParseIntegerOnly(true);

        testTv2.setText(mf.format(512.6445d));
        String result1 = NumberFormat.getInstance().format(12.23);
        testTv1.setText(result1);

        try {
            Number number = mf.parse("456.21");
            testTv3.setText(String.valueOf(number.intValue()));

            mf.setParseIntegerOnly(false);
            testTv4.setText(String.valueOf(mf.parse("123.456").intValue()));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        DecimalFormat df = new DecimalFormat("#.");
        df.setDecimalSeparatorAlwaysShown(false);
        testTv5.setText(df.format(123.456d));

    }
}
