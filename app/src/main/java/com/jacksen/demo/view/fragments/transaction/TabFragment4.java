package com.jacksen.demo.view.fragments.transaction;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jacksen.demo.view.R;

/**
 * Tab Fragment 4
 */
public class TabFragment4 extends Fragment {


    private View testView;

    public TabFragment4() {
    }

    /**
     * @return
     */
    public static TabFragment4 newInstance() {
        TabFragment4 fragment = new TabFragment4();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TabFragment4 ", "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        Log.d("TabFragment4 ", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("TabFragment4 ", "onCreateView");

        View view = inflater.inflate(R.layout.fragment_tab_fragment4, container, false);

        testView = view.findViewById(R.id.test_view);

        return view;
    }

    public void setBackground(String color) {
        testView.setBackgroundColor(Color.parseColor(color));
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TabFragment4", "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TabFragment4", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TabFragment4", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TabFragment4", "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TabFragment4 ", "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TabFragment4 ", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TabFragment4 ", "onDetach");
    }
}
