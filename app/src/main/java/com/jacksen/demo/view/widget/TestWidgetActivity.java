package com.jacksen.demo.view.widget;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

public class TestWidgetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_widget);
    }
}
