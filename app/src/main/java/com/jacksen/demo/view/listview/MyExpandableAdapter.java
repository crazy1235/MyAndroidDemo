package com.jacksen.demo.view.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import java.util.List;

/**
 * Created by Admin on 2015/12/18.
 */
public class MyExpandableAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<GroupBean> list;

    public MyExpandableAdapter(Context context, List<GroupBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return list.get(groupPosition).getChildList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return list.get(groupPosition).getChildList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupHolder groupHolder = null;
        if (convertView == null) {
            groupHolder = new GroupHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_expandable_group, null);
            groupHolder.tv = (TextView) convertView.findViewById(R.id.item_group_tv);

            convertView.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }
        groupHolder.tv.setText(list.get(groupPosition).getName());
        /*if (isExpanded) {
            Toast.makeText(context, "expanded", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "not expanded", Toast.LENGTH_SHORT).show();
        }*/
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildrenHolder childrenHolder = null;
        if (convertView == null) {
            childrenHolder = new ChildrenHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_expandable_children, null);
            childrenHolder.tv = (TextView) convertView.findViewById(R.id.item_children_tv);

            convertView.setTag(childrenHolder);
        } else {
            childrenHolder = (ChildrenHolder) convertView.getTag();
        }
        childrenHolder.tv.setText(list.get(groupPosition).getChildList().get(childPosition).getName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    /**
     *
     */
    class GroupHolder {
        public TextView tv;
    }

    /**
     *
     */
    class ChildrenHolder {
        public TextView tv;
    }
}
