package com.jacksen.demo.view.editText;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;

/**
 * 手机号自动分割和验证EditText
 * Created by ys on 2015/10/30.
 */
public class PhoneNumberEditText extends AppCompatEditText {

    /**
     * blank string
     */
    private final String BLANK = " ";

    private boolean isRun = false;
    public boolean isTel;

    public PhoneNumberEditText(Context context) {
        super(context);
        init();
    }

    public PhoneNumberEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PhoneNumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    /**
     *
     */
    private void init() {

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.d("text", "before" + ": " + "s=" + s.toString() + "  start=" + start + "  count=" + count + "  after=" + after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("text", "changed" + ": " + "s=" + s.toString() + "  start=" + start + "  count=" + count + "  before=" + before);
                /*if (null == s || s.length() == 0) {
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < s.length(); i++) {
                    sb.append(s.charAt(i));
                    if ((sb.length() == 4) || sb.length() == 9) {
                        sb.insert(sb.length() - 1, BLANK);
                    }
                }
                if (!sb.equals(s)) {
                    setText(sb.toString());
                }
*/
                if (isRun) {//这几句要加，不然每输入一个值都会执行两次onTextChanged()，导致堆栈溢出，原因不明
                    isRun = false;
                    return;
                }
                isRun = true;
                Log.i("tag", "onTextChanged()");
                if (isTel) {
                    String finalString = "";
                    int index = 0;
                    String telString = s.toString().replace(" ", "");
                    if ((index + 3) < telString.length()) {
                        finalString += (telString.substring(index, index + 3) + BLANK);
                        index += 3;
                    }
                    while ((index + 4) < telString.length()) {
                        finalString += (telString.substring(index, index + 4) + BLANK);
                        index += 4;
                    }
                    finalString += telString.substring(index, telString.length());
                    setText(finalString);
                    //此语句不可少，否则输入的光标会出现在最左边，不会随输入的值往右移动
                    setSelection(finalString.length());
                }

            }

            @Override
            public void afterTextChanged(Editable ss) {
                Log.d("text", "after" + ": " + "s = " + ss.toString());
                /*String s = ss.toString();
                if (null == s || s.length() == 0) {
                    return;
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < s.length(); i++) {
                    sb.append(s.charAt(i));
                    if ((sb.length() == 4) || sb.length() == 9) {
                        sb.insert(sb.length() - 1, BLANK);
                    }
                }
                if (!sb.equals(s)) {
                    setText(sb.toString());
                }*/
            }
        });

    }

}
