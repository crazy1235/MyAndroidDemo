package com.jacksen.demo.view.taggroup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestTagGroupActivity extends AppCompatActivity {

    private DynamicTagFlowLayout dynamicTagFlowLayout;
    private List<String> tags = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tag_group);


        dynamicTagFlowLayout = (DynamicTagFlowLayout) findViewById(R.id.dynamic_tag);
        dynamicTagFlowLayout.setOnTagItemClickListener(new DynamicTagFlowLayout.OnTagItemClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Toast.makeText(TestTagGroupActivity.this, tv.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        initData();
        dynamicTagFlowLayout.setTags(tags);


        findViewById(R.id.add_tag_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tags.add(tags.get(new Random().nextInt(tags.size() - 1)));
            }
        });
    }

    private void initData() {
        tags.add("阳哥你好！");
        tags.add("Android开发");
        tags.add("新闻热点");
        tags.add("热水进宿舍啦！");
        tags.add("I love you");
        tags.add("成都妹子");
        tags.add("新余妹子");
        tags.add("仙女湖");
        tags.add("创新工厂");
        tags.add("孵化园");
        tags.add("神州100发射");
        tags.add("有毒疫苗");
        tags.add("顶你阳哥阳哥");
        tags.add("Hello World");
        tags.add("闲逛的蚂蚁");
        tags.add("闲逛的蚂蚁");
        tags.add("闲逛的蚂蚁");
        tags.add("闲逛的蚂蚁");
        tags.add("闲逛的蚂蚁");
        tags.add("闲逛的蚂蚁");
    }
}
