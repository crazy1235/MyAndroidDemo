package com.jacksen.demo.view.window;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.jacksen.demo.view.R;

public class TestWindowActivity extends AppCompatActivity {

    private FloatingView mFloatingView;
    private FloatingCameraView mFloatingCameraView;
    private boolean mInCamera = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_window);

        mFloatingView = new FloatingView(getApplicationContext(), R.layout.view_floating_default);
        mFloatingCameraView = new FloatingCameraView(getApplicationContext());

        mFloatingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestWindowActivity.this, "mFloatingView", Toast.LENGTH_SHORT).show();
                openCameraView();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mFloatingView.isShown()) {
            mFloatingView.dismiss();
        }

        if (null != mFloatingCameraView && mFloatingCameraView.isShown()) {
            mFloatingCameraView.dismiss();
            mInCamera = false;
        }
    }

    /**
     * 开启前置摄像头预览
     */
    public void openCameraView() {
        if (mInCamera) {
            Toast.makeText(getApplicationContext(), "关闭摄像头", Toast.LENGTH_SHORT).show();
            mFloatingCameraView.dismiss();
        } else {
            if (!mFloatingCameraView.show()) {
                Toast.makeText(getApplicationContext(), "打开摄像头权限失败,请在系统设置打开摄像头权限", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(getApplicationContext(), "打开摄像头", Toast.LENGTH_SHORT).show();
        }
        mInCamera = !mInCamera;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mFloatingView.isShown()) {
            mFloatingView.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mFloatingView.isShown()) {
            mFloatingView.dismiss();
        }
        if (null != mFloatingCameraView) {
            if (mFloatingCameraView.isShown()) {
                mFloatingCameraView.dismiss();
            }
            mFloatingCameraView.release();
        }
    }
}
