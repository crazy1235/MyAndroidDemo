package com.jacksen.demo.view.spannableString;

import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.ClickableSpan;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.MaskFilterSpan;
import android.text.style.QuoteSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ScaleXSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuggestionSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * SpannableStringBuilder
 */
public class TestSpannableString extends AppCompatActivity implements View.OnClickListener {


    @Bind(R.id.test_span_tv)
    TextView testSpanTv;
    @Bind(R.id.span_spinner)
    Spinner spanSpinner;
    @Bind(R.id.do_it_btn)
    Button doItBtn;
    @Bind(R.id.test_span_et)
    EditText testSpanEt;

    private SpannableString spannableString;

    private SpannableString spannableString2;

    private String[] spans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_spannable_string);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        spannableString = new SpannableString(testSpanTv.getText());
        spannableString2 = new SpannableString(testSpanEt.getText());
        spans = getResources().getStringArray(R.array.spans);
        spanSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spans));
    }

    @OnClick(value = {R.id.do_it_btn})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.do_it_btn:
                switch ((String) spanSpinner.getSelectedItem()) {
                    case "ForegroundColorSpan":
                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                        spannableString.setSpan(foregroundColorSpan, 0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "BackgroundColorSpan":
                        BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(Color.CYAN);
                        spannableString.setSpan(backgroundColorSpan, 5, 9, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "AbsoluteSizeSpan":
                        AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(18);
                        spannableString.setSpan(absoluteSizeSpan, 10, 14, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "RelativeSizeSpan":
                        RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(2);
                        spannableString.setSpan(relativeSizeSpan, 15, 19, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "StyleSpan":
                        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
                        spannableString.setSpan(styleSpan, 20, 24, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        styleSpan = new StyleSpan(Typeface.ITALIC);
                        spannableString.setSpan(styleSpan, 25, 29, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        styleSpan = new StyleSpan(Typeface.BOLD_ITALIC);
                        spannableString.setSpan(styleSpan, 30, 34, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "UnderlineSpan":
                        UnderlineSpan underlineSpan = new UnderlineSpan();
                        spannableString.setSpan(underlineSpan, 35, 39, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "StrikethroughSpan":
                        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
                        spannableString.setSpan(strikethroughSpan, 40, 44, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "URLSpan":
                        URLSpan urlSpan = new URLSpan("http://blog.csdn.net/crazy1235");
                        spannableString.setSpan(urlSpan, 45, 49, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "ImageSpan":
                        Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
                        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                        ImageSpan imageSpan = new ImageSpan(drawable);
                        spannableString.setSpan(imageSpan, 50, 54, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "ScaleXSpan":
                        ScaleXSpan scaleXSpan = new ScaleXSpan(1.5f);
                        spannableString.setSpan(scaleXSpan, 55, 60, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "SubscriptSpan":
                        SubscriptSpan subscriptSpan = new SubscriptSpan();
                        spannableString.setSpan(subscriptSpan, 61, 65, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "SuperscriptSpan":
                        SuperscriptSpan superscriptSpan = new SuperscriptSpan();
                        spannableString.setSpan(superscriptSpan, 66, 70, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "BulletSpan":
                        BulletSpan bulletSpan = new BulletSpan(30, Color.RED);
                        spannableString.setSpan(bulletSpan, 0, spannableString.length() - 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "QuoteSpan":
                        QuoteSpan quoteSpan = new QuoteSpan(Color.BLACK);
                        spannableString.setSpan(quoteSpan, 0, spannableString.length() - 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;
                    case "AlignmentSpan":
                        AlignmentSpan.Standard standard = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_OPPOSITE);
                        spannableString.setSpan(standard, 0, spannableString.length() - 1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        testSpanTv.setText(spannableString);
                        break;


                    case "TypefaceSpan":
                        TypefaceSpan typefaceSpan = new TypefaceSpan("serif");
                        spannableString2.setSpan(typefaceSpan, 0, 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        typefaceSpan = new TypefaceSpan("monospace");
                        spannableString2.setSpan(typefaceSpan, 11, 20, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanEt.setText(spannableString2);
                        break;
                    case "MaskFilterSpan":
                        BlurMaskFilter blurMaskFilter = new BlurMaskFilter(10, BlurMaskFilter.Blur.NORMAL);//模糊
                        MaskFilterSpan maskFilterSpan = new MaskFilterSpan(blurMaskFilter);
                        spannableString2.setSpan(maskFilterSpan, 21, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        EmbossMaskFilter embossMaskFilter = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f, 6, 3.5f);//浮雕
                        maskFilterSpan = new MaskFilterSpan(embossMaskFilter);
                        spannableString2.setSpan(maskFilterSpan, 31, 40, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanEt.setText(spannableString2);
                        break;
                    case "ClickableSpan":
                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                Toast.makeText(TestSpannableString.this, "clicked", Toast.LENGTH_SHORT).show();
                            }
                        };
                        spannableString2.setSpan(clickableSpan, 41, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanEt.setText(spannableString2);
                        break;
                    case "SuggestionSpan":
                        SuggestionSpan suggestionSpan1 = new SuggestionSpan(this, new String[]{"123", "abc", "!@#$"}, SuggestionSpan.FLAG_MISSPELLED);
                        spannableString2.setSpan(suggestionSpan1, 51, 60, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        SuggestionSpan suggestionSpan2 = new SuggestionSpan(this, new String[]{"123", "abc", "!@#$"}, SuggestionSpan.FLAG_AUTO_CORRECTION);
                        spannableString2.setSpan(suggestionSpan2, 61, 70, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        SuggestionSpan suggestionSpan3 = new SuggestionSpan(this, new String[]{"123", "abc", "!@#$"}, SuggestionSpan.FLAG_EASY_CORRECT);
                        spannableString2.setSpan(suggestionSpan3, 71, 80, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanEt.setText(spannableString2);
                        break;
                    case "DynamicDrawableSpan":
                        DynamicDrawableSpan dynamicDrawableSpan = new DynamicDrawableSpan() {
                            @Override
                            public Drawable getDrawable() {
                                return getResources().getDrawable(R.mipmap.img1);
                            }
                        };
                        spannableString2.setSpan(dynamicDrawableSpan, 81, 90, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        testSpanEt.setText(spannableString2);
                        break;
                    case "EasyEditSpan":
                        break;
                }
                break;
        }
    }
}
