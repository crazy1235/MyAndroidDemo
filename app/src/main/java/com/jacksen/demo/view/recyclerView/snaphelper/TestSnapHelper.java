package com.jacksen.demo.view.recyclerView.snaphelper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * SnapHelper
 * <p>
 * https://developer.android.com/topic/libraries/support-library/revisions.html#24-2-0-api-updates
 *
 * @author jacksen
 */
public class TestSnapHelper extends AppCompatActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private Button btn;
    private LinearLayoutManager linearLayoutManager;
    private SnapHelperAdapter adapter;
    private OrientationHelper orientationHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_snap_helper);
        ButterKnife.bind(this);

        init();

        btn = (Button) findViewById(R.id.btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TestSnapHelper", "linearLayoutManager.getWidth():" + linearLayoutManager.getWidth());
                Log.d("TestSnapHelper", "linearLayoutManager.getPaddingLeft():" + linearLayoutManager.getPaddingLeft());
                Log.d("TestSnapHelper", "linearLayoutManager.getPaddingRight():" + linearLayoutManager.getPaddingRight());
                Log.d("TestSnapHelper", "linearLayoutManager.getPaddingTop():" + linearLayoutManager.getPaddingTop());
                Log.d("TestSnapHelper", "linearLayoutManager.getPaddingBottom():" + linearLayoutManager.getPaddingBottom());

                Log.d("TestSnapHelper", "linearLayoutManager.getChildCount():" + linearLayoutManager.getChildCount());
                Log.d("TestSnapHelper", "linearLayoutManager.getItemCount():" + linearLayoutManager.getItemCount());

                Log.d("TestSnapHelper", "!!!!!!!!!!!");

                View firstChildView = linearLayoutManager.getChildAt(0);
                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedLeft(firstChildView):" + linearLayoutManager.getDecoratedLeft(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedRight(firstChildView):" + linearLayoutManager.getDecoratedRight(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedTop(firstChildView):" + linearLayoutManager.getDecoratedTop(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedBottom(firstChildView):" + linearLayoutManager.getDecoratedBottom(firstChildView));

                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedMeasuredHeight(firstChildView):" + linearLayoutManager.getDecoratedMeasuredHeight(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getDecoratedMeasuredWidth(firstChildView):" + linearLayoutManager.getDecoratedMeasuredWidth(firstChildView));

                Log.d("TestSnapHelper", "linearLayoutManager.getLeftDecorationWidth(firstChildView):" + linearLayoutManager.getLeftDecorationWidth(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getRightDecorationWidth(firstChildView):" + linearLayoutManager.getRightDecorationWidth(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getTopDecorationHeight(firstChildView):" + linearLayoutManager.getTopDecorationHeight(firstChildView));
                Log.d("TestSnapHelper", "linearLayoutManager.getBottomDecorationHeight(firstChildView):" + linearLayoutManager.getBottomDecorationHeight(firstChildView));


                //
                Log.d("TestSnapHelper", "linearLayoutManager.getWidth() - linearLayoutManager.getPaddingLeft()- linearLayoutManager.getPaddingRight():" + (linearLayoutManager.getWidth() - linearLayoutManager.getPaddingLeft() - linearLayoutManager.getPaddingRight()));
                Log.d("TestSnapHelper", "orientationHelper.getTotalSpace():" + orientationHelper.getTotalSpace());
                Log.d("TestSnapHelper", "orientationHelper.getEnd():" + orientationHelper.getEnd());
                Log.d("TestSnapHelper", "orientationHelper.getDecoratedEnd(firstChildView):" + orientationHelper.getDecoratedEnd(firstChildView));

                Log.d("TestSnapHelper", "orientationHelper.getEndAfterPadding():" + orientationHelper.getEndAfterPadding());

                Log.d("TestSnapHelper", "orientationHelper.getStartAfterPadding():" + orientationHelper.getStartAfterPadding());
                Log.d("TestSnapHelper", "orientationHelper.getEndAfterPadding():" + orientationHelper.getEndPadding());

                Log.d("TestSnapHelper", "orientationHelper.getDecoratedMeasurement(firstChildView):" + orientationHelper.getDecoratedMeasurement(firstChildView));
                Log.d("TestSnapHelper", "orientationHelper.getDecoratedMeasurementInOther(firstChildView):" + orientationHelper.getDecoratedMeasurementInOther(firstChildView));


                Log.d("TestSnapHelper", "--------");

                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)
                        firstChildView.getLayoutParams();
                int decorateStart = linearLayoutManager.getDecoratedLeft(firstChildView) - params.leftMargin;
                Log.d("TestSnapHelper", "decorateStart:" + decorateStart);
            }
        });


    }

    /**
     *
     */
    private void init() {
        List<String> list = new ArrayList<>();
        list.add("http://ww2.sinaimg.cn/large/7a8aed7bjw1ezwgshzjpmj21ao1y0tf0.jpg");
        list.add("http://ww2.sinaimg.cn/large/7a8aed7bjw1ezvbmuqz9cj20hs0qoq6o.jpg");
        list.add("http://ww2.sinaimg.cn/large/7a8aed7bjw1ezgal5vpjfj20go0p0adq.jpg");
        list.add("http://ww1.sinaimg.cn/large/7a8aed7bgw1et6yio5s7rj20hs0qogop.jpg");
        list.add("http://ww2.sinaimg.cn/large/7a8aed7bjw1f0cw7swd9tj20hy0qogoo.jpg");
        list.add("http://ww3.sinaimg.cn/large/7a8aed7bgw1ewb2ytx5okj20go0p0jva.jpg");
        list.add("http://ww2.sinaimg.cn/large/7a8aed7bjw1exfffnlf2gj20hq0qoju9.jpg");

        adapter = new SnapHelperAdapter(this, list);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(adapter);

        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.d("TestSnapHelper", "newState:" + newState);
            }
        });
        recyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                Log.d("TestSnapHelper", velocityX + "--" + velocityY);
                return false;
            }
        });*/

        //
//        LinearSnapHelper snapHelper = new LinearSnapHelper();
//        snapHelper.attachToRecyclerView(recyclerView);

        JackSnapHelper jackSnapHelper = new JackSnapHelper(JackSnapHelper.TYPE_SNAP_END);
        jackSnapHelper.attachToRecyclerView(recyclerView);
//
//        recyclerView.setOnFlingListener(null);
//        new GravitySnapHelper(Gravity.START, false, null).attachToRecyclerView(recyclerView);

        orientationHelper = OrientationHelper.createHorizontalHelper(linearLayoutManager);

//        recyclerView.smoothScrollToPosition();
//        recyclerView.scrollToPosition(2);
    }
}
