package com.jacksen.demo.view.anim;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;

import com.jacksen.demo.view.R;

public class SceneAnimAfter extends AppCompatActivity {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flag = getIntent().getExtras().getInt("flag");
        switch (flag){
            case 0:
                getWindow().setEnterTransition(new Explode());
                break;
            case 1:
                getWindow().setEnterTransition(new Slide());
                break;
            case 2:
                getWindow().setEnterTransition(new Fade());
                getWindow().setExitTransition(new Fade());
                break;
            case 3:
                getWindow().setEnterTransition(new Slide());
                break;
        }
//        TransitionManager
//        Scene
//        TransitionInflater
        setContentView(R.layout.activity_scene_anim_after);
    }

}
