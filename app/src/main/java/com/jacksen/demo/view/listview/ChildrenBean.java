package com.jacksen.demo.view.listview;

/**
 * Created by jacksen on 2015/12/18.
 */
public class ChildrenBean {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
