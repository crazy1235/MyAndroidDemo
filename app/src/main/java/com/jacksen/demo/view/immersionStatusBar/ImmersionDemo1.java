package com.jacksen.demo.view.immersionStatusBar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.jacksen.demo.view.R;

/**
 * 沉浸式状态栏 --- 测试示例 1
 *
 * @author ys
 */
public class ImmersionDemo1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //透明状态栏
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //透明导航栏
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        setContentView(R.layout.activity_immersion_demo1);

        init();
    }

    private void init() {
        Toast.makeText(this, "213", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "sdss222~~!@#$0", Toast.LENGTH_SHORT).show();
    }
}
