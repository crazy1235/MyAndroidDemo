package com.jacksen.demo.view.recyclerView;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;

import com.jacksen.demo.view.R;

/**
 * 测试GridRecyclerView
 */
public class TestRecyclerView4 extends AppCompatActivity {

    private static final int DATASET_COUNT = 5;

    private GridRecyclerView gridRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_grid_recycler_view);

        gridRecyclerView = (GridRecyclerView) findViewById(R.id.test_grid_recycler_view);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridRecyclerView.setLayoutManager(gridLayoutManager);

//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        gridRecyclerView.setLayoutManager(linearLayoutManager);


//        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_item);
//        LayoutAnimationController controller = new LayoutAnimationController(animation);
//        recyclerView.setLayoutAnimation(controller);

        String[] dataSet = new String[DATASET_COUNT];
        for (int i = 0; i < DATASET_COUNT; i++) {
            dataSet[i] = "美女" + i + "号";
        }

        RecyclerAdapter2 adapter2 = new RecyclerAdapter2(this, dataSet);


        /*Animation animation = AnimationUtils.loadAnimation(this, R.anim.item_anim_alpha);
        GridLayoutAnimationController gridLayoutAnimationController = new GridLayoutAnimationController(animation);
        gridLayoutAnimationController.setDirection(GridLayoutAnimationController.DIRECTION_BOTTOM_TO_TOP | GridLayoutAnimationController.DIRECTION_RIGHT_TO_LEFT);
        gridLayoutAnimationController.setDirectionPriority(GridLayoutAnimationController.PRIORITY_ROW);
        gridRecyclerView.setLayoutAnimation(gridLayoutAnimationController);*/

        gridRecyclerView.setAdapter(adapter2);
    }
}
