package com.jacksen.demo.view.dispatch;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Admin on 2017/5/2.
 */

public class TestButton extends AppCompatButton {
    public TestButton(Context context) {
        super(context);
    }

    public TestButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TestButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        boolean flag = super.dispatchTouchEvent(event);
        Log.d("TestButton -- " + this.getTag(), "dispatchTouchEvent" + MotionEventUtil.getMotionEventString(event.getAction()) + " -- " + flag);
        return flag;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean flag = super.onTouchEvent(event);
        Log.d("TestButton -- " + this.getTag(), "onTouchEvent" + MotionEventUtil.getMotionEventString(event.getAction()) + " -- " + flag);
        return flag;
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        super.setOnTouchListener(l);
    }
}
