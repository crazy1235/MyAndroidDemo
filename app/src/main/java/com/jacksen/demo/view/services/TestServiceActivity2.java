package com.jacksen.demo.view.services;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.util.Constants;

public class TestServiceActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_service2);

        /*Intent intent = new Intent();
        intent.setPackage(getPackageName());
        intent.setAction(Constants.ACTION_SERVICE);
        bindService(intent, connection, Service.BIND_AUTO_CREATE);*/

        Intent startServiceIntent = new Intent(TestServiceActivity2.this, TestService.class);
        startServiceIntent.setAction(Constants.ACTION_SERVICE);
        startService(startServiceIntent);
    }


    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("TestService", "onServiceConnected -- " + service.toString());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("TestService", "onServiceDisconnected");
        }
    };

}
