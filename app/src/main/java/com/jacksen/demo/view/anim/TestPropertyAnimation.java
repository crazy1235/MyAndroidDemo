package com.jacksen.demo.view.anim;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.jacksen.demo.view.R;


/**
 * Test Property Animation
 *
 * @author jacksen
 */
public class TestPropertyAnimation extends AppCompatActivity implements View.OnClickListener {

    ImageView targetIv;
    ImageView targetIv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_property_animation);

        targetIv = findViewById(R.id.target_iv);
        targetIv2 = findViewById(R.id.target_iv2);
        targetIv2.setOnClickListener(this);
        findViewById(R.id.animator_btn).setOnClickListener(this);
        findViewById(R.id.animator_set_btn).setOnClickListener(this);
        findViewById(R.id.values_holder_btn).setOnClickListener(this);
        findViewById(R.id.value_animator_btn).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.animator_btn:
                ObjectAnimator animator = ObjectAnimator.ofFloat(targetIv, "alpha", 1.0f, 0.0f);
                animator.setDuration(1000);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        Log.d("TestPropertyAnimation", "AnimationStart");
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        Log.d("TestPropertyAnimation", "AnimationEnd");

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        Log.d("TestPropertyAnimation", "AnimationCancel");
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        Log.d("TestPropertyAnimation", "AnimationRepeat");
                    }
                });
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Log.d("TestPropertyAnimation", "AnimationUpdate");
                    }
                });
                animator.start();
                break;
            case R.id.values_holder_btn:
                PropertyValuesHolder valuesHolder1 = PropertyValuesHolder.ofFloat("translationX", 0f, targetIv.getMeasuredWidth());
                PropertyValuesHolder valuesHolder2 = PropertyValuesHolder.ofFloat("rotation", 0f, 360f);
                PropertyValuesHolder valuesHolder3 = PropertyValuesHolder.ofFloat("alpha", 1f, 0.2f, 1f);
                ObjectAnimator animator1 = ObjectAnimator.ofPropertyValuesHolder(targetIv, valuesHolder1, valuesHolder2, valuesHolder3);
                animator1.setDuration(1000);
                animator1.start();
                break;
            case R.id.animator_set_btn:

                AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.animator_set);
                set.setTarget(targetIv2);
                set.start();

               /* ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(targetIv, "rotation", 0f, 360f);
                objectAnimator1.setDuration(1000);
                objectAnimator1.setInterpolator(new BounceInterpolator());
                ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(targetIv2, "translationX", 0f, 500f);
                objectAnimator2.setInterpolator(new AnticipateInterpolator());
                objectAnimator2.setDuration(800);
                AnimatorSet set = new AnimatorSet();
                set.play(objectAnimator2).after(1000);
                set.play(objectAnimator1).after(objectAnimator2);
                set.start();*/
                break;
            case R.id.target_iv2:
                Toast.makeText(this, "oh oh !", Toast.LENGTH_SHORT).show();
                break;
            case R.id.value_animator_btn:
                ValueAnimator valueAnimator = ValueAnimator.ofInt(0, 100);
                valueAnimator.setDuration(1000);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int animatedValue = (int) animation.getAnimatedValue();
                        Log.d("TestPropertyAnimation", "animatedValue:" + animatedValue);
                    }
                });
                valueAnimator.start();
                break;
            default:
                break;
        }
    }
}
