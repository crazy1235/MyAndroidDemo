package com.jacksen.demo.view.actionBar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.jacksen.demo.view.R;
import com.jauker.widget.BadgeView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestActionBar extends AppCompatActivity {

    private MenuItem menuItem1 = null;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_action_bar);

        /*init();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view = findViewById(R.id.action_item2);
                BadgeView badgeView = new BadgeView(TestActionBar.this);
                badgeView.setTargetView(view);
//                badgeView.setBadgeGravity(Gravity.RIGHT | Gravity.TOP);
//                badgeView.setBadgeCount(10);
                badgeView.setBadgeCount(1);

            }
        }, 1000);*/
        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);
    }

    /**
     *
     */
    private void init() {
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = getSupportActionBar();

        BadgeView badgeView = new BadgeView(TestActionBar.this);
        badgeView.setTargetView(actionBar.getCustomView());
//                badgeView.setBadgeGravity(Gravity.RIGHT | Gravity.TOP);
        badgeView.setBadgeCount(1);
    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestActionBar.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_test_action_bar, menu);

        /* menuItem1 = menu.findItem(R.id.action_item2);
        view = menuItem1.getActionView();

        return true;*/

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                //do other operate

                this.finish();
                break;
            case R.id.action_item1:
                Toast.makeText(TestActionBar.this, "123", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_item2:
                Toast.makeText(TestActionBar.this, "asdf", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_item3:
                Toast.makeText(TestActionBar.this, "11111", Toast.LENGTH_SHORT).show();
                break;
           /* case R.id.action_item_share:
                ShareActionProvider provider = (ShareActionProvider) item.getActionProvider();
                provider.setShareIntent(getDefaultShareIntent());
                break;*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
//        setShowIconAtOverflow(menu);
        return super.onMenuOpened(featureId, menu);
    }

    /**
     * @return
     */
    private Intent getDefaultShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "123");
        intent.putExtra(Intent.EXTRA_TEXT, "abc");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    /**
     * 反射修改sHasPermanentMenuKey的值使overflow一直显示。
     */
    private void setOverflowShowingAlways() {
        ViewConfiguration viewConfig = ViewConfiguration.get(this);
        try {
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            menuKeyField.setAccessible(true);
            menuKeyField.setBoolean(viewConfig, false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置overflow的action显示图标
     */
    private void setShowIconAtOverflow(Menu menu) {
        if (!menu.getClass().getSimpleName().equals("MenuBuilder")) {
            return;
        }
        try {
            Method method = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
            method.setAccessible(true);
            method.invoke(menu, true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
