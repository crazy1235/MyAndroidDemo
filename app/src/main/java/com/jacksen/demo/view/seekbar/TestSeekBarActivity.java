package com.jacksen.demo.view.seekbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * SeekBar控制ListView滚动
 *
 * @author ys
 */
public class TestSeekBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_seek_bar);
    }

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestSeekBarActivity.class));
    }

}
