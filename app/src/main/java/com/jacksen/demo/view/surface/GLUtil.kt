package com.jacksen.demo.view.surface

import android.opengl.GLES20
import android.util.Log

object GLUtil {

    fun loadShader(type: Int, shaderCode: String): Int {
        // 根据类型创建shader
        val shader = GLES20.glCreateShader(type)

        // 上传和编译着色器源代码
        GLES20.glShaderSource(shader, shaderCode)

        // 编译着色器对象
        GLES20.glCompileShader(shader)

        // 获取着色器信息日志
        val shaderLog = GLES20.glGetShaderInfoLog(shader)
        Log.d("GLUtil", "编译log -- $shaderLog")

        val compileStatus = IntArray(1)
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0)
        return if (compileStatus[0] == GLES20.GL_FALSE) {
            Log.d("GLUtil", "编译失败 -- $type")
            // 删除 shader
            GLES20.glDeleteShader(shader)
            0
        } else {
            shader
        }
    }

}