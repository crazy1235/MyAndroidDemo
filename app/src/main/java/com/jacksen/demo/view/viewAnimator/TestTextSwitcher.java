package com.jacksen.demo.view.viewAnimator;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.jacksen.demo.view.R;

public class TestTextSwitcher extends AppCompatActivity {

    private TextSwitcher textSwitcher;

    private Button nextTextBtn;

    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_text_switcher);

        textSwitcher = (TextSwitcher) findViewById(R.id.text_switcher);
        nextTextBtn = (Button) findViewById(R.id.next_text_btn);

        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        Animation fadeOutAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_out);

        textSwitcher.setInAnimation(fadeInAnimation);
        textSwitcher.setOutAnimation(fadeOutAnimation);

        //视图工厂
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView t = new TextView(TestTextSwitcher.this);
                t.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                t.setTextColor(Color.RED);
                t.setTextSize(50);
                return t;
            }
        });

        /*TextView t1 = new TextView(TestTextSwitcher.this);
        t1.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        t1.setTextColor(Color.RED);
        t1.setTextSize(50);

        TextView t2 = new TextView(TestTextSwitcher.this);
        t2.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
        t1.setTextColor(Color.GREEN);
        t2.setTextSize(50);

        textSwitcher.addView(t1, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        textSwitcher.addView(t2, 1, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));*/

        nextTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textSwitcher.setText(String.valueOf(++i));
            }
        });
    }
}
