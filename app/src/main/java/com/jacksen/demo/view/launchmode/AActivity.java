package com.jacksen.demo.view.launchmode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.jacksen.demo.view.R;

public class AActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AActivity", "onCreate" + "--" + this.toString());
        setContentView(R.layout.activity_a);

        findViewById(R.id.a_launch_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(AActivity.this, AActivity.class);
//                startActivityForResult(intent, 0);
                finish();
            }
        });

        findViewById(R.id.a_launch_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AActivity.this, BActivity.class));
            }
        });

        findViewById(R.id.a_launch_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AActivity.this, CActivity.class));
            }
        });

        findViewById(R.id.a_launch_btn4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AActivity.this, AActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        findViewById(R.id.a_launch_btn5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AActivity.this, AActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        findViewById(R.id.a_launch_btn6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AActivity.this, AActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("AActivity", "onStart" + "--" + this.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("AActivity", "onResume" + "--" + this.toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("AActivity", "onPause" + "--" + this.toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("AActivity", "onStop" + "--" + this.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("AActivity", "onDestroy" + "--" + this.toString());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("AActivity", "onNewIntent" + "--" + this.toString());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("AActivity", "onRestart" + "--" + this.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("AActivity", "onActivityResult" + "--" + this.toString());
    }
}

