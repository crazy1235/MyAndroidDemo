package com.jacksen.demo.view.gesture;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by Admin on 2016/1/18.
 */
public class MyLinearLayout extends LinearLayout implements View.OnTouchListener {

    private GestureDetector gestureDetector;

    public MyLinearLayout(Context context) {
        this(context, null);
    }

    public MyLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnTouchListener(this);
        gestureDetector = new GestureDetector(context, new SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                Log.d("MyLinearLayout", e1.getRawX() + "--" + e1.getRawY());
                Log.d("MyLinearLayout", e2.getRawX() + "--" + e2.getRawY());
                Log.d("MyLinearLayout", velocityX + "--" + velocityY);

                return super.onFling(e1, e2, velocityX, velocityY);
            }

            @Override
            public boolean onDown(MotionEvent e) {
                Log.d("MyLinearLayout", "onDown");
                return super.onDown(e);
            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d("MyLinearLayout", "onTouch");
        gestureDetector.onTouchEvent(event);
        return true;
    }


}
