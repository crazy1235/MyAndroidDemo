package com.jacksen.demo.view.egl

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jacksen.demo.view.R

class TestEGLActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_egl)
    }
}
