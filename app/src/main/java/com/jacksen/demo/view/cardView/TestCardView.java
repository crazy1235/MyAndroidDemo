package com.jacksen.demo.view.cardView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

public class TestCardView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_card_view);

        init();
    }


    /**
     *
     */
    private void init() {

    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestCardView.class));
    }

}
