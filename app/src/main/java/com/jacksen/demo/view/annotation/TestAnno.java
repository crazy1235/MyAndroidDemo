package com.jacksen.demo.view.annotation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

import java.lang.annotation.Annotation;

public class TestAnno extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_anno);

    }
}
