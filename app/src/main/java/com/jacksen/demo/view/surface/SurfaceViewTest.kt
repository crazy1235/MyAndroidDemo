package com.jacksen.demo.view.surface

import android.opengl.GLSurfaceView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jacksen.demo.view.R

class SurfaceViewTest : AppCompatActivity() {

    private var glView: GLSurfaceView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_surface_view_test)
        glView = GLSurfaceView(this)
        glView?.setRenderer(MyGLRender(this))
        setContentView(glView)
    }

    override fun onResume() {
        super.onResume()
        glView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        glView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
