package com.jacksen.demo.view.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 2016/7/18.
 */

public class SystemParam implements Parcelable {
    private String param;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.param);
    }

    public SystemParam() {
    }

    protected SystemParam(Parcel in) {
        this.param = in.readString();
    }

    public static final Parcelable.Creator<SystemParam> CREATOR = new Parcelable.Creator<SystemParam>() {
        @Override
        public SystemParam createFromParcel(Parcel source) {
            return new SystemParam(source);
        }

        @Override
        public SystemParam[] newArray(int size) {
            return new SystemParam[size];
        }
    };
}
