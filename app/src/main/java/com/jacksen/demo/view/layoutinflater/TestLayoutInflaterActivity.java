package com.jacksen.demo.view.layoutinflater;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.jacksen.demo.view.R;

/**
 * LayoutInflater
 *
 * @author ys
 */
public class TestLayoutInflaterActivity extends AppCompatActivity {

    private LinearLayout rootLayout;

    private LinearLayout contentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_layout_inflater);

        rootLayout = (LinearLayout) findViewById(R.id.root_layout);
        contentLayout = (LinearLayout) findViewById(R.id.content_layout);

//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        LayoutInflater inflater = LayoutInflater.from(this);
        LayoutInflater inflater = getLayoutInflater();
//        View tempLayout = inflater.inflate(R.layout.test_layout, null);
//        View tempLayout = inflater.inflate(R.layout.test_layout, rootLayout, false);
        View tempLayout = inflater.inflate(R.layout.test_layout, rootLayout, true);

        Log.d("Test", "tempLayout.getLayoutParams() == null:" + (tempLayout.getLayoutParams() == null));

        Log.d("TestLayoutInflaterActiv", tempLayout.toString());
        Log.d("TestLayoutInflaterActiv", rootLayout.toString());

//        rootLayout.addView(tempLayout);
//        contentLayout.addView(tempLayout);
    }

}
