package com.jacksen.demo.view.sampledata

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jacksen.demo.view.R

class TestSampleData : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_sample_data)
    }
}
