package com.jacksen.demo.view.fragments.transaction;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class TestFragments extends AppCompatActivity implements TabFragment2.onBtnClickListener {

    @Bind(R.id.test_fragments_layout)
    FrameLayout testFragmentsLayout;
    private FragmentManager fragmentManager;
    private TabFragment1 tabFragment1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragments2);
        ButterKnife.bind(this);

        Log.d("TestFragments", "onCreate -- before add fragment");

        fragmentManager = getSupportFragmentManager();
        tabFragment1 = TabFragment1.newInstance();
        fragmentManager.beginTransaction().add(R.id.test_fragments_layout, tabFragment1, "tab1").addToBackStack("tab1").commit();
//        fragmentManager.beginTransaction().replace(R.id.test_fragments_layout, tabFragment1, "tab1").addToBackStack("tab1").commit();

        Log.d("TestFragments", "onCreate -- after add fragment");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TestFragments", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TestFragments", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("TestFragments", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("TestFragments", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TestFragments", "onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void removeFragment1() {
        fragmentManager.beginTransaction().remove(tabFragment1).addToBackStack("remove").commit();
//        fragmentManager.beginTransaction().remove(tabFragment1).commit();
    }

    @Override
    public void tellSth(String str) {
        ((TabFragment1) fragmentManager.findFragmentByTag("tab1")).showSth("hello: " + str);
    }

    @Override
    public void popFrag2() {
        boolean flag = fragmentManager.popBackStackImmediate("tab2", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Toast.makeText(this, "flag:" + flag, Toast.LENGTH_SHORT).show();
    }
}
