package com.jacksen.demo.view.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.util.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 测试service
 */
public class TestServiceActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.start_service_btn)
    Button startServiceBtn;
    @Bind(R.id.stop_service_btn)
    Button stopServiceBtn;
    @Bind(R.id.bind_service_btn)
    Button bindServiceBtn;
    @Bind(R.id.unbind_service_btn)
    Button unbindServiceBtn;
    @Bind(R.id.start_intentService_btn)
    Button startIntentServiceBtn;
    @Bind(R.id.stop_intentService_btn)
    Button stopIntentServiceBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_service);
        ButterKnife.bind(this);

        /*new MyHandler().post(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.d("TestServiceActivity", "123");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }

    /**
     *
     */
    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:

                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestServiceActivity.class));
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("TestService", "onServiceConnected -- " + service.toString());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("TestService", "onServiceDisconnected");
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Intent intent = new Intent();
//        intent.setAction(Constants.ACTION_SERVICE);
//        stopService(intent);
//        unbindService(connection);
        Log.d("TestService", "onDestroy");
    }

    @OnClick({R.id.start_service_btn, R.id.stop_service_btn, R.id.bind_service_btn, R.id.unbind_service_btn,
            R.id.start_intentService_btn, R.id.stop_intentService_btn})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_service_btn:
//                startActivity(new Intent(TestServiceActivity.this, TestServiceActivity2.class));
                Intent startServiceIntent = new Intent(TestServiceActivity.this, TestService.class);
                startServiceIntent.setAction(Constants.ACTION_SERVICE);
                startService(startServiceIntent);
                break;
            case R.id.stop_service_btn:
                Intent stopServiceIntent = new Intent(TestServiceActivity.this, TestService.class);
                stopServiceIntent.setAction(Constants.ACTION_SERVICE);
                stopService(stopServiceIntent);
                break;
            case R.id.bind_service_btn:
                Intent intent = new Intent();
                intent.setPackage(getPackageName());
                intent.setAction(Constants.ACTION_SERVICE);
                bindService(intent, connection, Service.BIND_AUTO_CREATE);
                break;
            case R.id.unbind_service_btn:
                unbindService(connection);
                break;
            case R.id.start_intentService_btn:
                startActivity(new Intent(TestServiceActivity.this, TestServiceActivity2.class));
               /* Intent intent1 = new Intent(TestServiceActivity.this, TestIntentService.class);
                intent1.setAction(TestIntentService.ServiceAction.ACTION_START_BEAT);
                startService(intent1);

                startIntentServiceBtn.setEnabled(false);
                stopIntentServiceBtn.setEnabled(true);*/
                break;
            case R.id.stop_intentService_btn:
                Intent intent2 = new Intent(TestServiceActivity.this, TestIntentService.class);
                intent2.setAction(TestIntentService.ServiceAction.ACTION_STOP_BEAT);
                startService(intent2);

                stopIntentServiceBtn.setEnabled(false);
                startIntentServiceBtn.setEnabled(true);
                break;
            default:

                break;
        }
    }
}
