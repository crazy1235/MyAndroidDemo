package com.jacksen.demo.view.anim;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * View Animation & Drawable Animation动画示例
 *
 * @author jacksen
 */
public class TestAnimation extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.scale_btn)
    Button scaleBtn;
    @Bind(R.id.alpha_btn)
    Button alphaBtn;
    @Bind(R.id.translate_btn)
    Button translateBtn;
    @Bind(R.id.rotate_btn)
    Button rotateBtn;
    @Bind(R.id.animation_btn)
    Button animationBtn;
    @Bind(R.id.target_iv)
    ImageView targetIv;
    @Bind(R.id.frame_anim_btn)
    Button frameAnimBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_animation);
        ButterKnife.bind(this);
    }


    @OnClick(value = {R.id.scale_btn, R.id.alpha_btn, R.id.translate_btn, R.id.rotate_btn, R.id.animation_btn, R.id.frame_anim_btn})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scale_btn:
                ScaleAnimation scaleAnimation = (ScaleAnimation) AnimationUtils.loadAnimation(this, R.anim.scale_anim);
                targetIv.startAnimation(scaleAnimation);

                /*ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
                scaleAnimation.setDuration(1000);
                scaleAnimation.setInterpolator(new OvershootInterpolator());
                scaleAnimation.setFillAfter(true);
                targetIv.startAnimation(scaleAnimation);*/

                break;
            case R.id.alpha_btn:
                /*AlphaAnimation alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(this, R.anim.alpha_anim);
                targetIv.startAnimation(alphaAnimation);*/

                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.2f);
                alphaAnimation.setDuration(1500);
                alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                alphaAnimation.setRepeatMode(Animation.REVERSE);
                alphaAnimation.setRepeatCount(1);
                targetIv.startAnimation(alphaAnimation);

                break;
            case R.id.translate_btn:
                /*Animation translateAnimation = AnimationUtils.loadAnimation(this, R.anim.translate_anim);
                targetIv.startAnimation(translateAnimation);*/


                TranslateAnimation translateAnimation = new TranslateAnimation(0, 400, 0, 0);
                translateAnimation.setDuration(1000);
                translateAnimation.setInterpolator(new AnticipateOvershootInterpolator());
                targetIv.startAnimation(translateAnimation);

                break;
            case R.id.rotate_btn:
                /*Animation rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_anim);
                targetIv.startAnimation(rotateAnimation);*/


                RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 550.0f, Animation.RELATIVE_TO_SELF, 0.3f, Animation.RELATIVE_TO_SELF, 0.3f);
                rotateAnimation.setDuration(1500);
                rotateAnimation.setInterpolator(new OvershootInterpolator());
                rotateAnimation.setFillAfter(true);
                targetIv.startAnimation(rotateAnimation);

                break;
            case R.id.animation_btn:
                /*Animation animation = AnimationUtils.loadAnimation(this, R.anim.set_anim);
                targetIv.startAnimation(animation);*/

                AnimationSet animationSet = new AnimationSet(true);
                animationSet.setDuration(2500);
                animationSet.setInterpolator(new AnticipateOvershootInterpolator());
                ScaleAnimation scaleAnim = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, Animation.RELATIVE_TO_PARENT, 0.5f, Animation.RELATIVE_TO_PARENT, 0.5f);
                AlphaAnimation alphaAnim = new AlphaAnimation(1.0f, 0.0f);
                TranslateAnimation translateAnim = new TranslateAnimation(0, 200, 0, 200);
                RotateAnimation rotateAnim = new RotateAnimation(0.0f, 550.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animationSet.addAnimation(scaleAnim);
                animationSet.addAnimation(alphaAnim);
                animationSet.addAnimation(translateAnim);
                animationSet.addAnimation(rotateAnim);
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        Toast.makeText(TestAnimation.this, "animationStart", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Toast.makeText(TestAnimation.this, "((AnimationSet)animation).getAnimations().size():" + ((AnimationSet) animation).getAnimations().size(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                targetIv.startAnimation(animationSet);
                break;
            case R.id.frame_anim_btn:
                targetIv.setImageDrawable(null);
                targetIv.setBackgroundResource(R.drawable.frame_anim);
                AnimationDrawable animationDrawable = (AnimationDrawable) targetIv.getBackground();
                animationDrawable.start();

               /* AnimationDrawable animationDrawable = new AnimationDrawable();
                animationDrawable.setOneShot(false);
                animationDrawable.addFrame(getResources().getDrawable(R.drawable.person1), 200);
                animationDrawable.addFrame(getResources().getDrawable(R.drawable.person2), 200);
                animationDrawable.addFrame(getResources().getDrawable(R.drawable.person3), 200);
                animationDrawable.addFrame(getResources().getDrawable(R.drawable.person4), 200);
                animationDrawable.addFrame(getResources().getDrawable(R.drawable.person5), 200);
                targetIv.setImageDrawable(animationDrawable);
                animationDrawable.start();*/

                break;
        }
    }
}
