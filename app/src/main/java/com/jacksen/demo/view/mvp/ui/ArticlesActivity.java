package com.jacksen.demo.view.mvp.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.mvp.presenter.ArticlePresenter;
import com.jacksen.demo.view.mvp.presenter.BasePresenter;
import com.jacksen.demo.view.mvp.view.ArticleViewInter;

/**
 * @author jacksen
 */
public class ArticlesActivity extends BaseActivity<ArticleViewInter, ArticlePresenter> implements ArticleViewInter {

    private ArticlePresenter presenter;

    @Override
    protected ArticlePresenter createPresenter() {
        presenter = new ArticlePresenter(this);
        return presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);

        presenter.fetchArticles();
    }


}
