package com.jacksen.demo.view.egl

import android.content.Context
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView

class SurfaceViewRenderer @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, style: Int = 0)
    : SurfaceView(context, attrs, style), SurfaceHolder.Callback {

    val eglHelper = EGLHelper()

    init {
        holder.addCallback(this)
    }


    override fun surfaceCreated(holder: SurfaceHolder) {
        eglHelper.setSurface(holder.surface)
        eglHelper.initEgl()
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        val inputStream = resources.assets.open("aa1.jpg")
        val bitmap = BitmapFactory.decodeStream(inputStream)
        eglHelper.drawBitmap(bitmap)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {

    }


}