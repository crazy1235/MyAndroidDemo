package com.jacksen.demo.view.dispatch;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

public class TestDispatchActivity extends AppCompatActivity {

    private LinearLayout rootLayout;

    private LinearLayout groupLayout;

    private Button button1, button2;

    private TextView textView1, textView2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_dispatch);

        initView();
    }

    private void initView() {
        rootLayout = (LinearLayout) findViewById(R.id.root_layout);
        groupLayout = (LinearLayout) findViewById(R.id.group_layout);
        button1 = (Button) findViewById(R.id.button1);
        textView1 = (TextView) findViewById(R.id.text_view1);
        button2 = (Button) findViewById(R.id.button2);
        textView2 = (TextView) findViewById(R.id.text_view2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TestDispatchActivity", "button1 -- onClick");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestDispatchActivity.this, "textView2 has clicked!", Toast.LENGTH_SHORT).show();

                if (textView2.getTouchDelegate() == null){
                    Rect bounds = new Rect(0, 0, button2.getMeasuredHeight(), button2.getMeasuredWidth());
                    Log.d("TestDispatchActivity", "button2.getMeasuredHeight():" + button2.getMeasuredHeight());
                    TouchDelegate touchDelegate = new TouchDelegate(bounds, button2);
                    textView2.setTouchDelegate(touchDelegate);
                }
            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d("TestDispatchActivity", "dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("TestDispatchActivity", "onTouchEvent");
        return super.onTouchEvent(event);
    }
}
