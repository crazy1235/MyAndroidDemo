package com.jacksen.demo.view.seekbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.icu.util.Measure;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.widget.TextView;

/**
 * Created by admin on 2017/11/2.
 */

public class ColorSeekBar extends android.support.v7.widget.AppCompatSeekBar {
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public ColorSeekBar(Context context) {
        this(context, null);
    }

    public ColorSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        paint.setColor(Color.BLACK);
        paint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, getResources().getDisplayMetrics()));


        setProgressDrawable(new ColorDrawable());

//        Drawable thumbDrawable = new android.graphics.drawable.ColorDrawable(Color.RED);

//        Drawable[] drawables = new Drawable[2];
//        drawables[0] = new ColorDrawable();
//        drawables[1] = thumbDrawable;
//        LayerDrawable layerDrawable = new LayerDrawable(drawables);

//        setThumb(thumbDrawable);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        /*LayerDrawable drawable = (LayerDrawable)getProgressDrawable();
        int layers = drawable.getNumberOfLayers();
        for (int i =0; i < layers; i++){
            if (drawable.getId(i) == android.R.id.background){
                drawable.setDrawableByLayerId(i, new ColorDrawable());
            }
        }*/
    }

    private String getText() {
        return getProgress() + "%";
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int height = MeasureSpec.getSize(heightMeasureSpec);

        Rect rect = new Rect();
        paint.getTextBounds(getText(), 0, getText().length(), rect);
        int width = rect.width();//文字宽
        int textHeight = rect.height();//文字高

        height += 40;

//        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), height);
//        int newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

//        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.getMode(heightMeasureSpec)));

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() + 150);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        Log.d("onLayout", "left: " + left);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        invalidate();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // draw text
        Rect rect = getProgressDrawable().getBounds();

        canvas.drawText(getText(), rect.width() * getProgress() / getMax(), 50, paint);

    }
}
