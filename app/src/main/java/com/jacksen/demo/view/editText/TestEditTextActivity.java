package com.jacksen.demo.view.editText;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.jacksen.demo.view.R;

/**
 * http://blog.csdn.net/u010358168/article/details/49421745
 * http://www.oschina.net/question/577632_85133
 */

public class TestEditTextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_edittext);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        init();
    }

    private void init() {
        MultiEditInputView idmeiv = (MultiEditInputView) findViewById(R.id.id_meiv);
        idmeiv.setHintText("hintText");
        idmeiv.setContentText("ContentText");

        Log.d("TestEditTextActivity", "onCreate: getHintText" + idmeiv.getHintText());
        Log.i("TestEditTextActivity", "onCreate: getContentText" + idmeiv.getContentText());
    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestEditTextActivity.class));
    }

}
