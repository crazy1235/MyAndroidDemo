package com.jacksen.demo.view.mvp.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.mvp.presenter.BasePresenter;

/**
 * Created by jacksen on 2016/3/21.
 */
public abstract class BaseActivity<V, P extends BasePresenter<V>> extends AppCompatActivity {

    protected P presenter;

    protected abstract P createPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        presenter.attachView((V) this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

}
