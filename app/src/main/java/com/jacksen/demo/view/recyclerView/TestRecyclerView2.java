package com.jacksen.demo.view.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jacksen.demo.view.R;

public class TestRecyclerView2 extends AppCompatActivity {

    private RecyclerView recyclerView;

    private static final int DATASET_COUNT = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_recycler_view2);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            init();
        }


    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestRecyclerView2.class));
    }

    /**
     *
     */
    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.test2_recyclerview);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);

//        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_item);
//        LayoutAnimationController controller = new LayoutAnimationController(animation);
//        recyclerView.setLayoutAnimation(controller);

        String[] dataSet = new String[DATASET_COUNT];
        for (int i = 0; i < DATASET_COUNT; i++) {
            dataSet[i] = "美女" + i + "号";
        }

        RecyclerAdapter2 adapter2 = new RecyclerAdapter2(this, dataSet);

//        LayoutAnimationController layoutAnimationController = new LayoutAnimationController();

        recyclerView.setAdapter(adapter2);
    }

}
