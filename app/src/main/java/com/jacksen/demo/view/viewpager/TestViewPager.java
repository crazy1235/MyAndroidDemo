package com.jacksen.demo.view.viewpager;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 测试ViewPager切换动画
 */
public class TestViewPager extends AppCompatActivity {

    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private int[] imgs = new int[]{R.drawable.jingtianming, R.drawable.xiangyu, R.drawable.shilan};

    private List<ImageView> imgList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_view_pager);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {
        viewPager.setAdapter(new MyPagerAdapter());
        viewPager.setPageTransformer(true, new DepthPageTransformer());
    }

    private class MyPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return imgs.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(TestViewPager.this);
            imageView.setBackgroundResource(imgs[position]);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imgList.add(imageView);
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(imgList.get(position));
        }
    }
}
