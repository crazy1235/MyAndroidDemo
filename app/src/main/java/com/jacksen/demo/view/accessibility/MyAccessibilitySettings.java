package com.jacksen.demo.view.accessibility;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

/**
 * accessibility 设置界面
 *
 * @author ys
 */
public class MyAccessibilitySettings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_accessibility_settings);
    }
}
