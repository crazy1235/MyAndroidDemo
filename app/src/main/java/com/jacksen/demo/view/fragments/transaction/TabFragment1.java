package com.jacksen.demo.view.fragments.transaction;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Tab Fragment 1
 */
public class TabFragment1 extends Fragment {


    @Bind(R.id.open_next_btn)
    Button openNextBtn;
    @Bind(R.id.get_sth_et)
    EditText getSthEt;

    public TabFragment1() {
    }

    /**
     *
     */
    public static TabFragment1 newInstance() {
        TabFragment1 fragment = new TabFragment1();
        return fragment;
    }

    public static TabFragment1 newInstance(String arg) {
        TabFragment1 fragment1 = new TabFragment1();
        Bundle bundle = new Bundle();
        bundle.putString("arg", arg);
        fragment1.setArguments(bundle);
        return fragment1;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TabFragment1", "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TabFragment1", "onCreate");
        if (getArguments() != null) {
            String arg = getArguments().getString("arg", "nothing");
            Toast.makeText(getActivity(), arg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("TabFragment1", "onCreateView");
        View view = inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        ButterKnife.bind(this, view);

        openNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                TabFragment2 tabFragment2 = TabFragment2.newInstance();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
//                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                fragmentTransaction.setTransitionStyle()
                fragmentTransaction.replace(R.id.test_fragments_layout, tabFragment2, "tab2");
//                fragmentTransaction.addToBackStack("tab2");
//                fragmentTransaction.add(R.id.test_fragments_layout, tabFragment2);
//                fragmentTransaction.addToBackStack("tab2");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TabFragment1", "onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TabFragment1", "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TabFragment1", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TabFragment1", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TabFragment1", "onStop");
    }

    /**
     * @param str
     */
    public void showSth(String str) {
        getSthEt.setText(str);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TabFragment1", "onDestroyView");
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TabFragment1", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TabFragment1", "onDetach");
    }
}
