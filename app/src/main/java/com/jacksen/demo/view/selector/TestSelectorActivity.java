package com.jacksen.demo.view.selector;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试各种selector
 */
public class TestSelectorActivity extends AppCompatActivity {

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_selector);

        gridView = (GridView) findViewById(R.id.grid_view);


        List<String> mList = new ArrayList<>();
        mList.add("金融");
        mList.add("互联网");
        mList.add("房地产");
        mList.add("教育");
        mList.add("政府机构");
        mList.add("文化娱乐");
        mList.add("医疗");
        mList.add("体育");
        mList.add("其他");


        GridViewAdapter adapter = new GridViewAdapter(this, mList);
        gridView.setAdapter(adapter);
    }
}
