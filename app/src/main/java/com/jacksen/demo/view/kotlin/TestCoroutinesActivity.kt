package com.jacksen.demo.view.kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.jacksen.demo.view.R
import kotlinx.coroutines.*

class TestCoroutinesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_coroutines)
    }

    fun test() {
        val job = GlobalScope.launch {

        }
    }
}
