package com.jacksen.demo.view.toolbar;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jacksen.demo.view.R;

/**
 * test toolbar
 *
 * @author ys
 */
public class TestToolBar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tool_bar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        /*if (actionBar != null) {
            actionBar.hide();
        }*/
        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setLogo(R.mipmap.ic_launcher);

        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setSubtitle("subtitle");
        toolbar.setSubtitleTextColor(Color.YELLOW);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case android.R.id.home:
                        Toast.makeText(TestToolBar.this, "home 111", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.action_item_search:

                        break;
                    case R.id.action_item_google:
                        Toast.makeText(TestToolBar.this, "google item has been clicked  111", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        MenuItem searchItem = menu.findItem(R.id.action_item_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        //
        MenuItem settingsItem = menu.findItem(R.id.action_item_settings);
        MenuItemCompat.setOnActionExpandListener(searchItem, expandListener);
        //expand the action view
//        searchItem.expandActionView();
        return super.onCreateOptionsMenu(menu);
    }


    /**
     *
     */
    private MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {
            Toast.makeText(TestToolBar.this, "onMenuItemActionExpand", Toast.LENGTH_SHORT).show();
            return true;//return true to expand action view
        }

        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            Toast.makeText(TestToolBar.this, "onMenuItemActionCollapse", Toast.LENGTH_SHORT).show();
            return true;//return true to collapse action view
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Toast.makeText(this, "home", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_item_search:

                break;
            case R.id.action_item_google:
                Toast.makeText(this, "google item has been clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_item_settings:
                Toast.makeText(this, "settings item has been clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }
}
