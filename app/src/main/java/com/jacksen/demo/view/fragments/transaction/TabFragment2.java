package com.jacksen.demo.view.fragments.transaction;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Tab Fragment 2
 */
public class TabFragment2 extends Fragment {


    @Bind(R.id.remove_fragment_btn)
    Button removeFragmentBtn;
    @Bind(R.id.tell_sth_btn)
    Button tellSthBtn;
    @Bind(R.id.open_next_btn)
    Button openNextBtn;
    @Bind(R.id.pop_this_btn)
    Button popBtn;

    private onBtnClickListener onBtnClickListener;

    public TabFragment2() {
    }

    /**
     *
     */
    public static TabFragment2 newInstance() {
        TabFragment2 fragment = new TabFragment2();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TabFragment2", "onAttach");
        if (context instanceof onBtnClickListener) {
            onBtnClickListener = (TabFragment2.onBtnClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onBtnClickListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TabFragment2", "onCreate");
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("TabFragment2", "onCreateView");
        View view = inflater.inflate(R.layout.fragment_tab_fragment2, container, false);
        ButterKnife.bind(this, view);

        removeFragmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnClickListener.removeFragment1();
            }
        });

        tellSthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnClickListener.tellSth("这是我要对你说的话！");
            }
        });

        openNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                TabFragment3 tabFragment3 = TabFragment3.newInstance();

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_top, R.anim.slide_out_to_top);
                fragmentTransaction.replace(R.id.test_fragments_layout, tabFragment3, "tab3");
                fragmentTransaction.addToBackStack("tab3");
                fragmentTransaction.commit();
            }
        });

        popBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnClickListener.popFrag2();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TabFragment2", "onActivityCreated");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TabFragment2", "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TabFragment2", "onResume");
    }

    /**
     * 回调接口
     */
    public interface onBtnClickListener {
        void removeFragment1();

        void tellSth(String str);

        void popFrag2();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TabFragment2", "onDestroyView");
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TabFragment2", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TabFragment2", "onDetach");
    }
}
