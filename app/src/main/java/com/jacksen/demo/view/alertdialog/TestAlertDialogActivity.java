package com.jacksen.demo.view.alertdialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import java.lang.ref.WeakReference;

/**
 * test alert dialog
 */
public class TestAlertDialogActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    private LinearLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_alert_dialog);

        rootLayout = (LinearLayout) findViewById(R.id.root_layout);

        LayoutInflater inflater = getLayoutInflater();
        LinearLayout tempLayout = (LinearLayout) inflater.inflate(R.layout.temp_layout, null);
        ImageView imageView = (ImageView) tempLayout.findViewById(R.id.image_view);
//        rootLayout.addView(imageView);
        TextView textView = (TextView) tempLayout.findViewById(R.id.text_view);
        tempLayout.removeView(textView);
        rootLayout.addView(textView);

        init();
    }

    private void init() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("AlertDialog")
                .setMessage("that was originally added here")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        TestAlertDialogActivity.this.finish();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.create().show();*/

//        progressDialog = ProgressDialog.show(this, "title", "loading...");

//        new TimeAsyncTask().execute();

        progressDialog = new SafeProgressDialog(this);
        progressDialog.setTitle("title");
        progressDialog.setMessage("loading...");
        progressDialog.show();

        MyHandler handler = new MyHandler(this);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 3000);

        /*new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }).start();*/

        handler.sendEmptyMessageDelayed(1, 1000);
//        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }*/
    }

    private static class MyHandler extends Handler {

        private WeakReference<TestAlertDialogActivity> reference;

        public MyHandler(TestAlertDialogActivity activity) {
            this.reference = new WeakReference<TestAlertDialogActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == 1 && reference.get() != null && !reference.get().isFinishing()) {
                reference.get().finish();
            }
        }
    }


    private class TimeAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            TestAlertDialogActivity.this.finish();
        }
    }

    private class SafeProgressDialog extends ProgressDialog {

        AppCompatActivity activity;

        public SafeProgressDialog(Context context) {
            super(context);
            this.activity = (AppCompatActivity) context;
        }

        @Override
        public void dismiss() {
            if (activity != null && !activity.isFinishing()) {
                super.dismiss();
            }
        }
    }
}
