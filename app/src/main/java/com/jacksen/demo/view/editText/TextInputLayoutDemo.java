package com.jacksen.demo.view.editText;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * TextInputLayout 测试demo
 *
 * @author ys
 */
public class TextInputLayoutDemo extends AppCompatActivity {

    @Bind(R.id.usernameEt)
    EditText usernameEt;
    @Bind(R.id.usernameLayout)
    TextInputLayout usernameLayout;
    @Bind(R.id.passwordEt)
    EditText passwordEt;
    @Bind(R.id.passwordLayout)
    TextInputLayout passwordLayout;
    @Bind(R.id.btn)
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_input_layout_demo);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {

    }

    @OnClick(R.id.btn)
    void login() {
        if (!"admin".equals(usernameEt.getText().toString())) {
            usernameEt.setError("username is error");
            return;
        }
        if ("admin".equals(passwordEt.getText().toString())) {
            passwordEt.setError("password is error");
            return;
        }
    }


    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TextInputLayoutDemo.class));
    }

}
