package com.jacksen.demo.view.listview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author jacksen
 */
public class TestExpandableListView extends AppCompatActivity {

    @Bind(R.id.test_expandable_lv)
    ExpandableListView testExpandableLv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_expandable_list_view);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {
        List<GroupBean> groupBeans = new ArrayList<>();
        List<ChildrenBean> childrenBeans = new ArrayList<>();
        GroupBean groupBean = new GroupBean();
        ChildrenBean childrenBean = new ChildrenBean();
        childrenBean.setName("sd1");
        childrenBeans.add(childrenBean);


        childrenBean = new ChildrenBean();
        childrenBean.setName("sd2");
        childrenBeans.add(childrenBean);

        childrenBean = new ChildrenBean();
        childrenBean.setName("sd3");
        childrenBeans.add(childrenBean);


        childrenBean = new ChildrenBean();
        childrenBean.setName("sd4");
        childrenBeans.add(childrenBean);

        groupBean.setName("group1");
        groupBean.setChildList(childrenBeans);
        groupBeans.add(groupBean);


        groupBean = new GroupBean();
        groupBean.setName("group2");
        groupBean.setChildList(childrenBeans);
        groupBeans.add(groupBean);

        groupBean = new GroupBean();
        groupBean.setName("group3");
        groupBean.setChildList(childrenBeans);
        groupBeans.add(groupBean);


        //
        MyExpandableAdapter adapter = new MyExpandableAdapter(this, groupBeans);
        testExpandableLv.setAdapter(adapter);
        testExpandableLv.setGroupIndicator(null);

        testExpandableLv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Toast.makeText(TestExpandableListView.this, groupPosition + "--" + id, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        testExpandableLv.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(TestExpandableListView.this, "setOnGroupExpandListener -- groupPosition:" + groupPosition, Toast.LENGTH_SHORT).show();
            }
        });

        testExpandableLv.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(TestExpandableListView.this, "setOnGroupCollapseListener -- groupPosition:" + groupPosition, Toast.LENGTH_SHORT).show();
            }
        });

        testExpandableLv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(TestExpandableListView.this, groupPosition + "--" + childPosition + "--" + id, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

    }
}
