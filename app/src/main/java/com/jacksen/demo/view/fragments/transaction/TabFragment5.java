package com.jacksen.demo.view.fragments.transaction;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jacksen.demo.view.R;

/**
 * test fragment 5
 *
 * @author jacksen
 */
public class TabFragment5 extends Fragment {

    public TabFragment5() {
    }

    public static TabFragment5 newInstance() {
        TabFragment5 fragment = new TabFragment5();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TabFragment5", "onAttach");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TabFragment5", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("TabFragment5", "onCreateView");
        return inflater.inflate(R.layout.fragment_tab_fragment5, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("TabFragment5", "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TabFragment5", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TabFragment5", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TabFragment5", "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TabFragment5", "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TabFragment5", "onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TabFragment5", "onDetach");
    }
}
