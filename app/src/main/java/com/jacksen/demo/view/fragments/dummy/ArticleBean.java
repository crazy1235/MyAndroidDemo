package com.jacksen.demo.view.fragments.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 */
public class ArticleBean {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<ArticleItem> ITEMS = new ArrayList<ArticleItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, ArticleItem> ITEM_MAP = new HashMap<String, ArticleItem>();

    private static final int COUNT = 10;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(ArticleItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static ArticleItem createDummyItem(int position) {
        return new ArticleItem(String.valueOf(position), "article " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class ArticleItem {
        public final String id;
        public final String content;
        public final String details;

        public ArticleItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
