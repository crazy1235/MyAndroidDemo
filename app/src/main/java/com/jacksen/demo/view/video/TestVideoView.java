package com.jacksen.demo.view.video;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.fragments.ArticleActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * VideoView
 */
public class TestVideoView extends AppCompatActivity {

    @Bind(R.id.video_view)
    VideoView videoView;
    @Bind(R.id.start_btn)
    Button startBtn;

    private int videoPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_video_view);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {
        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.introduce));
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestVideoView.this, ArticleActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.resume();
        videoView.seekTo(videoPosition);
        videoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
        videoPosition = videoView.getCurrentPosition();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
