package com.jacksen.demo.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.fragments.dummy.ArticleBean;
import com.jacksen.demo.view.fragments.dummy.ArticleBean.ArticleItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnChangeArticleListener}
 * interface.
 */
public class HeadlinesFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 2;
    private OnChangeArticleListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HeadlinesFragment() {
    }

    public static HeadlinesFragment newInstance(int columnCount) {
        HeadlinesFragment fragment = new HeadlinesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("HeadlinesFragment", "onAttach");
        if (context instanceof OnChangeArticleListener) {
            mListener = (OnChangeArticleListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeArticleListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("HeadlinesFragment", "onCreate");

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("HeadlinesFragment", "onCreateView");
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new HeadlinesRecyclerViewAdapter(ArticleBean.ITEMS, mListener));
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("HeadlinesFragment", "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("HeadlinesFragment", "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("HeadlinesFragment", "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("HeadlinesFragment", "onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("HeadlinesFragment", "onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("HeadlinesFragment", "onDestroy()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("HeadlinesFragment", "onDetach");
        mListener = null;
    }

    /**
     *
     */
    public interface OnChangeArticleListener {
        void onChangeArticle(ArticleItem item);
    }
}
