package com.jacksen.demo.view.anim;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

//https://github.com/DreaminginCodeZH/Douya
public class TestSceneAnimation extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.img1)
    ImageView img1;
    @Bind(R.id.img2)
    ImageView img2;
    @Bind(R.id.explode_btn)
    Button explodeBtn;
    @Bind(R.id.slide_btn)
    Button slideBtn;
    @Bind(R.id.fade_btn)
    Button fadeBtn;
    @Bind(R.id.share_btn)
    Button shareBtn;
    @Bind(R.id.floating_btn)
    FloatingActionButton floatingBtn;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scene_animation);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @OnClick(value = {R.id.explode_btn, R.id.slide_btn, R.id.fade_btn, R.id.share_btn})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.explode_btn:
                intent = new Intent(TestSceneAnimation.this, SceneAnimAfter.class);
                intent.putExtra("flag", 0);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                break;
            case R.id.slide_btn:
                intent = new Intent(TestSceneAnimation.this, SceneAnimAfter.class);
                intent.putExtra("flag", 1);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                break;
            case R.id.fade_btn:
                intent = new Intent(TestSceneAnimation.this, SceneAnimAfter.class);
                intent.putExtra("flag", 2);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                break;
            case R.id.share_btn:
                intent = new Intent(TestSceneAnimation.this, SceneAnimAfter.class);
                intent.putExtra("flag", 3);
                startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this, floatingBtn, "share").toBundle());
                break;
            default:
                break;
        }
    }
}
