package com.jacksen.demo.view.mvp.presenter;

import android.content.Context;

import com.jacksen.demo.view.mvp.view.ArticleViewInter;
import com.jacksen.demo.view.mvp.view.BaseViewInter;

/**
 * Created by jacksen on 2016/3/21.
 */
public class ArticlePresenter extends BasePresenter<ArticleViewInter> {

    private ArticleViewInter articleViewInter;

    public ArticlePresenter(Context context) {
        articleViewInter = (ArticleViewInter) context;
    }

    /**
     *
     */
    public void fetchArticles() {
        if (!isViewAttached()) {
            throw new RuntimeException("not attached.");
        }


    }
}
