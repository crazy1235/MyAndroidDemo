package com.jacksen.demo.view.anim;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TestAnimActivity extends AppCompatActivity {

    @Bind(R.id.textView1)
    TextView textView1;
    @Bind(R.id.floating_action_button)
    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_object_anim);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.jump_from_down);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        textView1.startAnimation(animation);


        textView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return false;
            }
        });


        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
//        ObjectAnimator.ofPropertyValuesHolder()
//        OvershootInterpolator
//        PathInterpolator
//        BounceInterpolator
//        CycleInterpolator
    }

}
