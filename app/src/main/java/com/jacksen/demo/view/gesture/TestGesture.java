package com.jacksen.demo.view.gesture;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TestGesture extends AppCompatActivity {

    @Bind(R.id.showOrHideBtn)
    Button showOrHideBtn;
    @Bind(R.id.linear_layout1)
    LinearLayout linearLayout1;
    @Bind(R.id.linear_layout2)
    MyLinearLayout linearLayout2;


    private float oldRawY = 0f;
    private float newRawY = 0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_gesture);
        ButterKnife.bind(this);


        linearLayout1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        oldRawY = event.getRawY();
                        Log.d("TestGesture", "oldRawY:" + oldRawY);
                        break;
                    case MotionEvent.ACTION_UP:
                        newRawY = event.getRawY();
                        Log.d("TestGesture", "newRawY:" + newRawY);
                        if (Math.abs(newRawY - oldRawY) < 50) {
                            Toast.makeText(TestGesture.this, "onClick", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        Log.d("TestGesture", "event.getRawY():" + event.getRawY());
                        if (event.getRawY() - oldRawY >= 100) {//下滑
                            Log.d("TestGesture", "down");
                        } else if (oldRawY - event.getRawY() >= 100) {//上滑
                            Log.d("TestGesture", "up");
                        }
                        break;
                }
                return true;
            }
        });

        linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestGesture.this, "onClick", Toast.LENGTH_SHORT).show();
            }
        });

        showOrHideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestGesture.this, "button clicked", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
