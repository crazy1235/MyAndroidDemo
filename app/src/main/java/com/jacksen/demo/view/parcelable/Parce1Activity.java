package com.jacksen.demo.view.parcelable;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jacksen.demo.view.R;

import java.util.ArrayList;

import static com.jacksen.demo.view.parcelable.Parce2Activity.KEY_PARCE;
import static com.jacksen.demo.view.parcelable.Parce2Activity.KEY_PARCE_LIST;

public class Parce1Activity extends AppCompatActivity {

    private Button openNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parce1);

        openNextBtn = (Button) findViewById(R.id.open_next_btn);
        openNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Parce1Activity.this, Parce2Activity.class);
                StudentBean studentBean = new StudentBean("sololo", 1, 24, 175, "中国北京", true);

                ArrayList<StudentBean> studentBeans = new ArrayList<>();
                studentBeans.add(studentBean);
                studentBeans.add(studentBean);
                studentBeans.add(studentBean);

                intent.putExtra(KEY_PARCE, studentBean);
                intent.putParcelableArrayListExtra(KEY_PARCE_LIST, studentBeans);

                startActivity(intent);
            }
        });
    }
}
