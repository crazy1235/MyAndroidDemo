package com.jacksen.demo.view.recyclerView.snaphelper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jacksen.demo.view.R;

import java.util.List;

/**
 * @author jacksen
 *         <br/>
 * @since 2016/11/20
 */

public class SnapHelperAdapter extends RecyclerView.Adapter<SnapHelperAdapter.ItemViewHolder> {

    private Context context;

    private List<String> list;

    public SnapHelperAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_snap_helper, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Glide.with(context).load(list.get(position)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     *
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_view);
        }
    }


}
