package com.jacksen.demo.view.auto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *
 */
public class TestAutoCompleteTextView extends AppCompatActivity {

    @Bind(R.id.auto_textView)
    AutoCompleteTextView autoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_auto_complete_text_view);
        ButterKnife.bind(this);

        autoTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<String> listString = new ArrayList<String>();
                listString.add("1234");
                listString.add("asdbas");
                listString.add("`1234");
                listString.add("122234");
                listString.add("2334234");
                listString.add("654131dsf  dsfsd");
                ArrayAdapter<String> aAdapter = new ArrayAdapter<String>(
                        TestAutoCompleteTextView.this, android.R.layout.simple_list_item_1
                        , listString);
                autoTextView.setAdapter(aAdapter);
                aAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
