package com.jacksen.demo.view.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.fragments.transaction.TabFragment1;
import com.jacksen.demo.view.fragments.transaction.TabFragment2;
import com.jacksen.demo.view.fragments.transaction.TabFragment3;
import com.jacksen.demo.view.fragments.transaction.TabFragment4;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragment组成TAB页面
 *
 * @author ys
 */
public class TabActivity extends AppCompatActivity implements View.OnClickListener {

    @Bind(R.id.tab1_btn)
    Button tab1Btn;
    @Bind(R.id.tab2_btn)
    Button tab2Btn;
    @Bind(R.id.tab3_btn)
    Button tab3Btn;
    @Bind(R.id.tab4_btn)
    Button tab4Btn;
    @Bind(R.id.tab_content_layout)
    FrameLayout tabFrameLayout;

    private FragmentManager fragmentManager;

    private TabFragment1 tabFragment1;
    private TabFragment2 tabFragment2;
    private TabFragment3 tabFragment3;
    private TabFragment4 tabFragment4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        ButterKnife.bind(this);

        init();
    }

    /**
     *
     */
    private void init() {
        fragmentManager = getSupportFragmentManager();
        tabFragment1 = TabFragment1.newInstance();
        tabFragment2 = TabFragment2.newInstance();
        tabFragment3 = TabFragment3.newInstance();
        tabFragment4 = TabFragment4.newInstance();

        fragmentManager.beginTransaction().add(R.id.tab_content_layout, tabFragment1).addToBackStack("tab1").commit();

       /* fragmentManager.beginTransaction().add(R.id.tab_content_layout, tabFragment3).addToBackStack("tab3").commit();
        fragmentManager.beginTransaction().add(R.id.tab_content_layout, tabFragment4).addToBackStack("tab4").commit();*/

        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Log.d("TabActivity", "onBackStackChanged" + " --BackStackEntryCount: " + fragmentManager.getBackStackEntryCount());
            }
        });
    }


    @OnClick(value = {R.id.tab1_btn, R.id.tab2_btn, R.id.tab3_btn, R.id.tab4_btn})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_btn:
                /*if (tabFragment1 == null) {
                    tabFragment1 = TabFragment1.newInstance();
                }
                fragmentManager.beginTransaction().replace(R.id.tab_content_layout, tabFragment1, "tab1").addToBackStack("tab1").commit();
                Log.d("TabActivity", "fragmentManager.getBackStackEntryCount():" + fragmentManager.getBackStackEntryCount());*/
//                FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(0);

                fragmentManager.beginTransaction().add(R.id.tab_content_layout, tabFragment2).addToBackStack("tab2").commit();


                break;
            case R.id.tab2_btn:
                if (tabFragment2 == null) {
                    tabFragment2 = TabFragment2.newInstance();
                }
                fragmentManager.beginTransaction().replace(R.id.tab_content_layout, tabFragment2, "tab2").commit();
                break;
            case R.id.tab3_btn:
                if (tabFragment3 == null) {
                    tabFragment3 = TabFragment3.newInstance();
                }
                fragmentManager.beginTransaction().replace(R.id.tab_content_layout, tabFragment3, "tab3").commit();
                break;
            case R.id.tab4_btn:
                /*if (tabFragment4 == null) {
                    tabFragment4 = TabFragment4.newInstance();
                }
                fragmentManager.beginTransaction().replace(R.id.tab_content_layout, tabFragment4, "tab4").commitAllowingStateLoss();*/
                Log.d("TabActivity", "fragmentManager.getBackStackEntryCount():" + fragmentManager.getBackStackEntryCount());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
