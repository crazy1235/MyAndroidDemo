package com.jacksen.demo.view.kotlin

import android.content.Context
import android.graphics.Path
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.startActivity

/**
 * Created by admin on 2017/11/29.
 */
class TestKotlinActivity : AppCompatActivity() {

//    val mList: ArrayList<Any>? = null

    val uri = Uri.parse("") // normally
//    val mUri = "".toUri() // ktx

    var list = arrayListOf("1", "2", "abc", "jacksen")


    private val name: String by Preference(this, "name", "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        mList = kotlin.intArrayOf(1, 2, 3)

        getSharedPreferences("", Context.MODE_PRIVATE).edit().putBoolean("flag", false).apply()

//        getSharedPreferences("", Context.MODE_PRIVATE).edit{
//            putBoolean("flag", true)
//        }
//        uri?.let( :: testLet)

        list.forEach(::println)

        //
        startActivity<TestAnkoActivity>()
    }

    private fun testLet(uri: String) {

    }


    private fun testNewFun() {
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}