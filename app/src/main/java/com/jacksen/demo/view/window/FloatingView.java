package com.jacksen.demo.view.window;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Admin on 2017/5/26.
 */

public class FloatingView extends BaseFloatingView {

    private boolean isShowing = false;

    public FloatingView(@NonNull Context context, int viewResId) {
        super(context);
        View.inflate(context, viewResId, this);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "hhh", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 显示悬浮球
     */
    public void show() {
        if (!isShowing) super.showView(this);
        isShowing = true;
    }

    /**
     * 关闭悬浮球
     */
    public void dismiss() {
        if (isShowing)
            super.hideView();
        isShowing = false;
    }



}
