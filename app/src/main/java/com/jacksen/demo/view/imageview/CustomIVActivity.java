package com.jacksen.demo.view.imageview;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.jacksen.demo.view.R;

public class CustomIVActivity extends AppCompatActivity {

    private ImageView imageView;
    private ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_iv);

        imageView = (ImageView) findViewById(R.id.image_view);
        imageView2 = (ImageView) findViewById(R.id.image_view2);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.child);

        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable.getPaint().setAntiAlias(true);
        roundedBitmapDrawable.setCornerRadius(30);
        imageView.setImageDrawable(roundedBitmapDrawable);

        //

        RoundedBitmapDrawable roundedBitmapDrawable2 = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        roundedBitmapDrawable2.getPaint().setAntiAlias(true);
        roundedBitmapDrawable2.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()));
        imageView2.setImageDrawable(roundedBitmapDrawable2);
    }
}
