package com.jacksen.demo.view.mvp.bean;

/**
 * Created by jacksen on 2016/3/21.
 */
public class ArticleBean {

    private String title;
    private String author;
    private String content;
    private String dateline;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateline() {
        return dateline;
    }

    public void setDateline(String dateline) {
        this.dateline = dateline;
    }
}
