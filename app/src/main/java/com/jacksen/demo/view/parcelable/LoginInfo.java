package com.jacksen.demo.view.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 2016/7/18.
 */

public class LoginInfo implements Parcelable {

    private int index;
    private String operFlag;
    private boolean isMobile;
    private List<UserInfo> userInfos;
    private SystemParam systemParam;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.index);
        dest.writeString(this.operFlag);
        dest.writeByte(this.isMobile ? (byte) 1 : (byte) 0);
        dest.writeList(this.userInfos);
        dest.writeParcelable(this.systemParam, flags);
    }

    public LoginInfo() {
    }

    protected LoginInfo(Parcel in) {
        this.index = in.readInt();
        this.operFlag = in.readString();
        this.isMobile = in.readByte() != 0;
        this.userInfos = new ArrayList<UserInfo>();
        in.readList(this.userInfos, UserInfo.class.getClassLoader());
        this.systemParam = in.readParcelable(SystemParam.class.getClassLoader());
    }

    public static final Parcelable.Creator<LoginInfo> CREATOR = new Parcelable.Creator<LoginInfo>() {
        @Override
        public LoginInfo createFromParcel(Parcel source) {
            return new LoginInfo(source);
        }

        @Override
        public LoginInfo[] newArray(int size) {
            return new LoginInfo[size];
        }
    };
}


class UserInfo {
    private int userId;
    private String nickName;
    private long birthday;
    private String userType;
}
