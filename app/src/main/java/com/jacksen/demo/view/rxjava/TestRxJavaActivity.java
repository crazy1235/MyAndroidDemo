package com.jacksen.demo.view.rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.jacksen.demo.view.R;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


/**
 * RxJava
 */
public class TestRxJavaActivity extends AppCompatActivity {

    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_rx_java);

        observable.subscribe(observer);

    }

    Observer observer = new Observer() {
        @Override
        public void onSubscribe(Disposable disposable) {
            Log.d("TestRxJavaActivity", "onSubscribe");

            disposable.dispose();
        }

        @Override
        public void onNext(Object o) {
            Log.d("TestRxJavaActivity", "onNext");
        }

        @Override
        public void onError(Throwable throwable) {
            Log.d("TestRxJavaActivity", "onError");
        }

        @Override
        public void onComplete() {
            Log.d("TestRxJavaActivity", "onComplete");
        }
    };

    Observable observable = Observable.create(new ObservableOnSubscribe() {
        @Override
        public void subscribe(ObservableEmitter emitter) throws Exception {
            emitter.onNext("hello");
            emitter.onNext("world");
            emitter.onComplete();
            emitter.onNext("other");
        }
    });

}
