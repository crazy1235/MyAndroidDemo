package com.jacksen.demo.view.launchmode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.jacksen.demo.view.R;

public class BActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("BActivity", "onCreate" + "--" + this.toString());
        setContentView(R.layout.activity_b);

        findViewById(R.id.b_launch_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BActivity.this, AActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        findViewById(R.id.b_launch_btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BActivity.this, BActivity.class));
            }
        });

        findViewById(R.id.b_launch_btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BActivity.this, CActivity.class));
            }
        });

        findViewById(R.id.b_launch_btn4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BActivity.this, AActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        findViewById(R.id.b_launch_btn5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BActivity.this, AActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("BActivity", "onStart" + "--" + this.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("BActivity", "onResume" + "--" + this.toString());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("BActivity", "onPause" + "--" + this.toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("BActivity", "onStop" + "--" + this.toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("BActivity", "onDestroy" + "--" + this.toString());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("BActivity", "onNewIntent" + "--" + this.toString());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("BActivity", "onRestart" + "--" + this.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("BActivity", "onActivityResult" + "--" + this.toString());
    }

}
