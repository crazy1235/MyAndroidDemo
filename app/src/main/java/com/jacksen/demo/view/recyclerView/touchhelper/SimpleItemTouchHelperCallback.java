package com.jacksen.demo.view.recyclerView.touchhelper;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

/**
 * Created by admin on 2017/11/23.
 */

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private ItemTouchHelperListener touchHelperListener;

    public SimpleItemTouchHelperCallback(ItemTouchHelperListener touchHelperListener) {
        this.touchHelperListener = touchHelperListener;
    }

    /**
     * 设置是否可以左右滑动或者上下拖动
     *
     * @param recyclerView
     * @param viewHolder
     * @return
     */
    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN; // 允许上下拖动
        int swipeFlags = ItemTouchHelper.LEFT; // 只允许从右向左滑动
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    /**
     * 拖动item上下移动的时候回调此方法
     *
     * @param recyclerView
     * @param viewHolder
     * @param target
     * @return
     */
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        touchHelperListener.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    /**
     * 当左右滑动item达到删除出发条件时，会回调此方法
     * 如果不在这个函数内做adapter移除item操作的话，它仅仅是将item的视图移除屏幕，会有一片空白区域
     *
     * @param viewHolder
     * @param direction
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        touchHelperListener.onItemDismiss(viewHolder.getAdapterPosition());
    }
}
