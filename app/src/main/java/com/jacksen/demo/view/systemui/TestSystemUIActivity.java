package com.jacksen.demo.view.systemui;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.jacksen.demo.view.R;

public class TestSystemUIActivity extends AppCompatActivity {

    private CheckBox visibleChkBox;
    private CheckBox lowProfileChkBox;
    private CheckBox hideNavigationChkBox;
    private CheckBox fullscreenChkBox;
    private CheckBox layoutStableChkBox;
    private CheckBox layoutHideNavigationChkBox;
    private CheckBox layoutFullscreenChkBox;
    private CheckBox flagImmersiveChkBox;
    private CheckBox flagImmersiveStickyChkBox;
    private CheckBox lightStatusBarChkbox;

    private String TAG = TestSystemUIActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_system_ui);

        visibleChkBox = (CheckBox) findViewById(R.id.visibleChkBox);
        lowProfileChkBox = (CheckBox) findViewById(R.id.lowProfileChkBox);
        hideNavigationChkBox = (CheckBox) findViewById(R.id.hideNavigationChkBox);
        fullscreenChkBox = (CheckBox) findViewById(R.id.fullscreenChkBox);
        layoutStableChkBox = (CheckBox) findViewById(R.id.layoutStableChkBox);
        layoutHideNavigationChkBox = (CheckBox) findViewById(R.id.layoutHideNavigationChkBox);
        layoutFullscreenChkBox = (CheckBox) findViewById(R.id.layoutFullscreenChkBox);
        flagImmersiveChkBox = (CheckBox) findViewById(R.id.flagImmersiveChkBox);
        flagImmersiveStickyChkBox = (CheckBox) findViewById(R.id.flagImmersiveStickyChkBox);
        lightStatusBarChkbox = (CheckBox) findViewById(R.id.lightStatusBarChkbox);

        findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWindow().getDecorView().setSystemUiVisibility(makeFlag());
                getSystemUiStatus();
            }
        });

        findViewById(R.id.hideSystemUIBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSystemUI();
            }
        });

        findViewById(R.id.showSystemUIBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSystemUI();
            }
        });

        findViewById(R.id.toggleHideyBar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHideyBar();
            }
        });
    }

    private void getSystemUiStatus() {
        int mSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
        Toast.makeText(this, "mSystemUiVisibility: " + mSystemUiVisibility, Toast.LENGTH_SHORT).show();
    }

    private int makeFlag() {
        int flag = View.SYSTEM_UI_FLAG_VISIBLE;
        if (visibleChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_VISIBLE;
        }
        if (lowProfileChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
        }
        if (hideNavigationChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (fullscreenChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (layoutStableChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        }
        if (layoutHideNavigationChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
        }
        if (layoutFullscreenChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        }
        if (flagImmersiveChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_IMMERSIVE;
        }
        if (flagImmersiveStickyChkBox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY; // from api level 19 -- 4.4
        }
        if (lightStatusBarChkbox.isChecked()) {
            flag |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR; // from api level 23 -- 6.0
        }
        return flag;
    }


    public static void setNavigationbarHide(AppCompatActivity activity, boolean hide) {
        View decor = activity.getWindow().getDecorView();
        if (hide && Build.VERSION.SDK_INT > 18) {
            decor.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else if (hide && Build.VERSION.SDK_INT > 15) {
            decor.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        } else if (hide && Build.VERSION.SDK_INT > 13) {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        } else {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        View mDecorView = getWindow().getDecorView();
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View mDecorView = getWindow().getDecorView();
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_VISIBLE);
    }


    /**
     * Detects and toggles immersive mode (also known as "hidey bar" mode).
     */
    public void toggleHideyBar() {
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        boolean isImmersiveModeEnabled = ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.i(TAG, "Turning immersive mode mode off. ");
        } else {
            Log.i(TAG, "Turning immersive mode mode on.");
        }
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }
}
