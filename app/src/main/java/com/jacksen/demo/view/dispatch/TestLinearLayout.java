package com.jacksen.demo.view.dispatch;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * @author jacksen
 */

public class TestLinearLayout extends LinearLayout {

    public TestLinearLayout(Context context) {
        super(context);
    }

    public TestLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TestLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        boolean flag = super.dispatchTouchEvent(event);
        Log.d("TestLinearLayout -- " + this.getTag(), "dispatchTouchEvent" + MotionEventUtil.getMotionEventString(event.getAction()) + " -- " + flag);
        return flag;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean flag = super.onInterceptTouchEvent(event);
        Log.d("TestLinearLayout -- " + this.getTag(), "onInterceptTouchEvent" + MotionEventUtil.getMotionEventString(event.getAction()) + " -- " + flag);
        return flag;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean flag = super.onTouchEvent(event);
        Log.d("TestLinearLayout -- " + this.getTag(), "onTouchEvent" + MotionEventUtil.getMotionEventString(event.getAction()) + " -- " + flag);
        return flag;
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        super.setOnTouchListener(l);
    }

}
