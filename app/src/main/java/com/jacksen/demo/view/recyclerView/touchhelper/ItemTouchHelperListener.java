package com.jacksen.demo.view.recyclerView.touchhelper;

/**
 * Created by ys on 2017/11/23.
 */

public interface ItemTouchHelperListener {

    /**
     * 交换item
     */
    void onItemMove(int fromPosition, int toPosition);

    /**
     * 删除item
     *
     * @param position item的position
     */
    void onItemDismiss(int position);
}
