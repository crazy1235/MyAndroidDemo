package com.jacksen.demo.view.mvp.presenter;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 * Created by jacksen on 2016/3/21.
 */
public abstract class BasePresenter<V> {

    protected Reference<V> viewPrefer;

    /**
     * 建立关联
     *
     * @param view
     */
    public void attachView(V view) {
        viewPrefer = new WeakReference<V>(view);
    }


    /**
     * 获取view
     *
     * @return
     */
    protected V getView() {
        return viewPrefer.get();
    }


    /**
     * 判断是否与view建立了关联
     *
     * @return
     */
    public boolean isViewAttached() {
        return viewPrefer != null && viewPrefer.get() != null;
    }

    /**
     * 解除关联
     */
    public void detachView() {
        if (viewPrefer != null) {
            viewPrefer.clear();
            viewPrefer = null;
        }
    }
}
