package com.jacksen.demo.view.optimization;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

import java.util.HashMap;

/**
 * 性能优化篇~
 * ArrayMap
 * SparseArray
 *
 * @author jacksen
 */
public class OptimizationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optimization);


        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("1", "first");
        hashMap.put("2", "second");
        hashMap.put("3", "bb");
        hashMap.put(null, "c");

//        SparseArray<String> sparseArray = new SparseArray<>();

    }
}
