package com.jacksen.demo.view.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 2016/7/18.
 */

public class StudentBean implements Parcelable{

    private String name;
    private int sex;
    private int age;
    private int height;
    private String address;
    private boolean flag;

    public StudentBean(String name, int sex, int age, int height, String address, boolean flag) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.height = height;
        this.address = address;
        this.flag = flag;
    }

    public StudentBean() {
    }

    protected StudentBean(Parcel in) {
        name = in.readString();
        sex = in.readInt();
        age = in.readInt();
        height = in.readInt();
        address = in.readString();
        flag = in.readByte() != 0;
    }

    public static final Creator<StudentBean> CREATOR = new Creator<StudentBean>() {
        @Override
        public StudentBean createFromParcel(Parcel in) {
            return new StudentBean(in);
        }

        @Override
        public StudentBean[] newArray(int size) {
            return new StudentBean[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(sex);
        dest.writeInt(age);
        dest.writeInt(height);
        dest.writeString(address);
        dest.writeByte((byte) (flag ? 1 : 0));
    }
}
