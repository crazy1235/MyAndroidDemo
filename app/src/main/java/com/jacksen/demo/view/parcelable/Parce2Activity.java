package com.jacksen.demo.view.parcelable;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import java.util.ArrayList;

public class Parce2Activity extends AppCompatActivity {

    public static final String KEY_PARCE = "student_info";
    public static final String KEY_PARCE_LIST = "student_info_list";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parce2);


        Intent intent = getIntent();
        StudentBean studentBean = intent.getExtras().getParcelable(KEY_PARCE);
        if (studentBean == null) {
            return;
        }

        ArrayList<StudentBean> studentBeans = intent.getExtras().getParcelableArrayList(KEY_PARCE_LIST);
        Toast.makeText(this, "studentBeans.size():" + studentBeans.size(), Toast.LENGTH_SHORT).show();

//        Log.d("Parce2Activity", intent.getExtras().getString(KEY_PARCE_LIST));
        Log.d("Parce2Activity", intent.getStringExtra(KEY_PARCE_LIST));
        Log.d("Parce2Activity", "intent.getStringArrayListExtra(KEY_PARCE_LIST):" + intent.getStringArrayListExtra(KEY_PARCE_LIST));

        Toast.makeText(this, studentBean.getAddress(), Toast.LENGTH_SHORT).show();


    }
}
