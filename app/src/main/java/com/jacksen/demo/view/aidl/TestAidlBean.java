package com.jacksen.demo.view.aidl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 2017/8/31.
 */

public class TestAidlBean implements Parcelable {

    private String name;
    private int age;

    public TestAidlBean(String name, int age) {
        this.name = name;
        this.age = age;
    }

    protected TestAidlBean(Parcel in) {
        name = in.readString();
        age = in.readInt();
    }

    public static final Creator<TestAidlBean> CREATOR = new Creator<TestAidlBean>() {
        @Override
        public TestAidlBean createFromParcel(Parcel in) {
            return new TestAidlBean(in);
        }

        @Override
        public TestAidlBean[] newArray(int size) {
            return new TestAidlBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
    }
}
