package com.jacksen.demo.view.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.fragments.dummy.ArticleBean;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment {

    private static final String ARTICLE_ID = "ARTICLE_ID";
    private static final String ARTICLE_TITLE = "ARTICLE_TITLE";


    private String articleId;//文章ID
    private String articleTitle = "i am a empty fragment.";//文章title

    private TextView titleTv;

    private OnFragmentInteractionListener mListener;

    public ArticleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param articleId    Parameter 1.
     * @param articleTitle Parameter 2.
     * @return A new instance of fragment ArticleFragment.
     */
    public static ArticleFragment newInstance(String articleId, String articleTitle) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putString(ARTICLE_ID, articleId);
        args.putString(ARTICLE_TITLE, articleTitle);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     *
     * @return
     */
    public static ArticleFragment newInstance() {
        ArticleFragment fragment = new ArticleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            articleId = getArguments().getString(ARTICLE_ID);
            articleTitle = getArguments().getString(ARTICLE_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        titleTv = (TextView) view.findViewById(R.id.article_title_tv);
        titleTv.setText(articleTitle);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * 更新文章的视图
     *
     * @param item
     */
    public void updateArticleView(ArticleBean.ArticleItem item) {
//        ArticleBean.ArticleItem articleItem = ArticleBean.ITEMS.get(position);
//        titleTv.setText(articleItem.details);
        titleTv.setText(item.details);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
