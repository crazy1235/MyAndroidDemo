package com.jacksen.demo.view.recyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.Toast;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.recyclerView.touchhelper.SimpleItemTouchHelperCallback;

import java.util.Arrays;

public class TestRecyclerView extends AppCompatActivity {

    private RecyclerView recyclerView;

    private static final int DATASET_COUNT = 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_recycler_view);

        Log.d("TestRecyclerView", "onCreate");

        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TestRecyclerView", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TestRecyclerView", "onResume");
    }

    /**
     *
     */
    private void init() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        String[] dataSet = new String[DATASET_COUNT];
        for (int i = 0; i < DATASET_COUNT; i++) {
            dataSet[i] = "This is element #" + i;
        }

        RecyclerAdapter1 adapter = new RecyclerAdapter1(Arrays.asList(dataSet));

        /*Animation animation = AnimationUtils.loadAnimation(this, R.anim.item_list_anim);
        LayoutAnimationController layoutAnimationController = new LayoutAnimationController(animation);
        LayoutAnimationController.AnimationParameters animationParameters = new GridLayoutAnimationController.AnimationParameters();
        layoutAnimationController.setInterpolator(new AccelerateInterpolator());
        layoutAnimationController.setDelay(0.5f);
        layoutAnimationController.setOrder(LayoutAnimationController.ORDER_RANDOM);
        recyclerView.setLayoutAnimation(layoutAnimationController);*/
        recyclerView.setLayoutAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Toast.makeText(TestRecyclerView.this, "layout animation end", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutAnimation(layoutAnimationController);

        SimpleItemTouchHelperCallback helperCallback = new SimpleItemTouchHelperCallback(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(helperCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestRecyclerView.class));
    }

}
