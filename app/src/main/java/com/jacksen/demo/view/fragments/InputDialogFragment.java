package com.jacksen.demo.view.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jacksen.demo.view.R;

/**
 * A simple {@link DialogFragment} subclass.
 */
public class InputDialogFragment extends DialogFragment {


    private EditText inputEt;
    private Button confirmBtn;

    public InputDialogFragment() {
        // Required empty public constructor
    }


    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("this is a title.");
//        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉title
        View view = inflater.inflate(R.layout.fragment_test_dialog, container, false);

        inputEt = (EditText) view.findViewById(R.id.input_name_et);
        confirmBtn = (Button) view.findViewById(R.id.confirm_btn);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "my name :" + inputEt.getText().toString(), Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });
        return view;
    }*/

    @NonNull
     @Override
     public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_test_dialog, null);
        final EditText et = (EditText) view.findViewById(R.id.input_name_et);
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), et.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }
}
