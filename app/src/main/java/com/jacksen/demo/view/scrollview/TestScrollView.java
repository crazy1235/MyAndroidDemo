package com.jacksen.demo.view.scrollview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import com.jacksen.demo.view.R;

/**
 * scroll view
 *
 * @author jacksen
 */
public class TestScrollView extends AppCompatActivity {

    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scroll_view);

        scrollView = (ScrollView) findViewById(R.id.scroll_view);


    }
}
