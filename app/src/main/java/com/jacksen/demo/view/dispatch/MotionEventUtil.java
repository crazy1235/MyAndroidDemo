package com.jacksen.demo.view.dispatch;

import android.view.MotionEvent;

/**
 * Created by Admin on 2017/5/2.
 */

public class MotionEventUtil {

    public static String getMotionEventString(int action) {
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                return "ACTION_DOWN";
            case MotionEvent.ACTION_MOVE:
                return "ACTION_MOVE";
            case MotionEvent.ACTION_UP:
                return "ACTION_UP";
            default:
                return String.valueOf(action);
        }
    }
}
