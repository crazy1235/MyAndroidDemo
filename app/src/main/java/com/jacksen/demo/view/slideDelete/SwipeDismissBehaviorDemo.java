package com.jacksen.demo.view.slideDelete;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SwipeDismissBehaviorDemo extends AppCompatActivity {

    @Bind(R.id.delete_tv)
    TextView deleteTv;
    @Bind(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_dismiss_behavior_demo);
        ButterKnife.bind(this);

        //
        SwipeDismissBehavior<View> swipe = new SwipeDismissBehavior<>();

        swipe.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY);
        swipe.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view) {

            }

            @Override
            public void onDragStateChanged(int state) {

            }
        });
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) deleteTv.getLayoutParams();
        layoutParams.setBehavior(swipe);
    }
}
