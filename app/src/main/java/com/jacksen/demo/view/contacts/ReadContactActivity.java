package com.jacksen.demo.view.contacts;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 读取联系人
 *
 * @author ys
 */
public class ReadContactActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_CONTACT = 001;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(android.R.id.edit)
    EditText edit;
    @Bind(R.id.search_contact_btn)
    Button searchContactBtn;
    @Bind(android.R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_contact);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, REQUEST_CODE_CONTACT);
            }
        });
    }

    @OnClick(R.id.search_contact_btn)
    void clickBtn() {
        String ss = null;
        System.out.println(ss);
    }

    /**
     *
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, ReadContactActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (REQUEST_CODE_CONTACT == requestCode) {
            if (data.getData() != null) {
                Uri uri = data.getData();
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                int count = cursor.getCount();//行数
                int columnCount = cursor.getColumnCount();//列数
                String names[] = cursor.getColumnNames();//列名
                Toast.makeText(this, "count:" + count, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "names:" + names.length, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "columnCount:" + columnCount, Toast.LENGTH_SHORT).show();
//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < names.length; i++) {
//                    sb.append(names[i] + "\n");
//                }
//                Log.d("ReadContactActivity", sb.toString());
                cursor.moveToFirst();
                Toast.makeText(this, "count:" + count, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "names:" + names.length, Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "columnCount:" + columnCount, Toast.LENGTH_SHORT).show();
                int _id = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                if (_id != -1) {
                    Log.d("ReadContactActivity", cursor.getString(_id));
                }

                //
                cursor.close();
            }
        }
    }
}
