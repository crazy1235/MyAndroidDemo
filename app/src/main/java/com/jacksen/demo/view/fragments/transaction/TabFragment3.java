package com.jacksen.demo.view.fragments.transaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.fragments.InputDialogFragment;
import com.jacksen.demo.view.preference.TestPreferenceActivity;

/**
 * Tab Fragment 3
 */
public class TabFragment3 extends Fragment {
    private Button showDialogFragment;

    private Button setPreBtn;

    public TabFragment3() {

    }

    /**
     * @return
     */
    public static TabFragment3 newInstance() {
        TabFragment3 fragment = new TabFragment3();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab_fragment3, container, false);

        showDialogFragment = (Button) view.findViewById(R.id.show_dialog_fragment);

        showDialogFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputDialogFragment inputDialogFragment = new InputDialogFragment();
                inputDialogFragment.show(getFragmentManager(), "inputDialogFragment");
            }
        });

        setPreBtn = (Button) view.findViewById(R.id.set_preference_btn);
        setPreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TestPreferenceActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
