package com.jacksen.demo.view.listview.simple;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jacksen.demo.view.R;


public class TestItemZoom extends AppCompatActivity {
    private LvListView lvListView;
    private LvAdapter lvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_lv);
        lvListView = (LvListView) findViewById(R.id.lvListView);
        String[] strs = {"", "请加入试剂一", "开始预温", "开始检测", "检测完成", "打印结果", ""};
        lvAdapter = new LvAdapter(strs, this);
        lvListView.setAdapter(lvAdapter);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                lvListView.scrollToItem();
                break;
        }
    }
}
