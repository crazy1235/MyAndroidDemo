package com.jacksen.demo.view.gridview;

import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.GridLayoutAnimationController;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class TestGridViewActivity extends AppCompatActivity {

    private GridView gridView;
    private List<ResolveInfo> app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_grid_view);

        gridView = (GridView) findViewById(R.id.test_grid_view);

        init();
    }

    private void init() {
        List<HashMap<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            HashMap<String, Object> item = new HashMap<>();
            item.put("img", R.drawable.img12);
            item.put("dest", i);
            list.add(item);
        }

        SimpleAdapter adapter = new SimpleAdapter(this, list, R.layout.item_test_recyclerview2, new String[]{"img", "dest"}, new int[]{R.id.item_iv, R.id.item_textview});

        gridView.setAdapter(adapter);

//        GridLayoutAnimationController


        /*Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        app = getPackageManager().queryIntentActivities(intent, 0);


        gridView.setAdapter(new GridAdapter());*/
    }

    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return Math.min(app.size(), 30);
        }

        @Override
        public Object getItem(int position) {
            return app.get(position % app.size());
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(TestGridViewActivity.this);
            ResolveInfo info = app.get(position % app.size());
            i.setImageDrawable(info.activityInfo.loadIcon(getPackageManager()));
            i.setScaleType(ImageView.ScaleType.FIT_CENTER);
            final int w = (int) (36 * getResources().getDisplayMetrics().density + 0.5f);
            i.setLayoutParams(new GridView.LayoutParams(w, w));
            return i;
        }
    }
}
