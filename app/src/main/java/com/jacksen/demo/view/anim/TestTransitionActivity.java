package com.jacksen.demo.view.anim;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.jacksen.demo.view.R;

/**
 * transition
 *
 * @author jacksen
 */
public class TestTransitionActivity extends AppCompatActivity {

    private Button goNextBtn;

    private ImageView childIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_transition);

        goNextBtn = (Button) findViewById(R.id.go_next_btn);

        childIv = (ImageView) findViewById(R.id.child_iv);

        goNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestTransitionActivity.this, TransitionResultActivity.class);
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(TestTransitionActivity.this, childIv, getString(R.string.name_transition));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startActivity(intent, optionsCompat.toBundle());
                }
            }
        });


        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Transition transition = TransitionInflater.from(this).inflateTransition(R.transition.activity_fade);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setExitTransition(transition);
            }
        }*/
    }
}
