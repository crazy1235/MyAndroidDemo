package com.jacksen.demo.view.permission;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 申请权限，授予权限测试
 *
 * @author jacksen
 */
public class TestPermission extends AppCompatActivity implements View.OnClickListener {

    private static final int CODE_REQUEST_CALL_PHONE = 001;

    @Bind(R.id.makeCallBtn)
    Button makeCallBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_permission);
        ButterKnife.bind(this);

        Log.d("TestPermission", "TestPermission -- onCreate");
    }

    @OnClick(value = R.id.makeCallBtn)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.makeCallBtn:
                if (ActivityCompat.checkSelfPermission(TestPermission.this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(TestPermission.this, Manifest.permission.READ_CONTACTS)) {
                        Toast.makeText(this, "shouldShowRequestPermissionRationale", Toast.LENGTH_SHORT).show();
                    } else {
                        ActivityCompat.requestPermissions(TestPermission.this, new String[]{Manifest.permission.CALL_PHONE}, CODE_REQUEST_CALL_PHONE);
                    }
                } else {
                    makeCall();
                }
                break;
            default:
                break;
        }
    }

    private void makeCall() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri uri = Uri.parse("tel:" + "10086");
        intent.setData(uri);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (CODE_REQUEST_CALL_PHONE == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {
                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("TestPermission", "TestPermission -- onDestroy");
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Log.d("TestPermission", "onAttachedToWindow");
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.d("TestPermission", "onDetachedFromWindow");
    }
}
