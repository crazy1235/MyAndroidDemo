package com.jacksen.demo.view.listview.simple;

import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jacksen.demo.view.R;

/**
 * Created by lv on 2015/1/15.
 */
public class LvAdapter extends BaseAdapter {
    private String[] strs;
    private Context context;
    private boolean b = true;

    public LvAdapter(String[] atrs, Context context) {
        this.strs = atrs;
        this.context = context;
    }

    @Override
    public int getCount() {
        return strs.length;
    }

    @Override
    public Object getItem(int position) {
        return strs[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHold viewHold;
        if (convertView == null) {
            viewHold = new ViewHold();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }
        viewHold.textView = (TextView) convertView.findViewById(R.id.textview);
        viewHold.textView.setText(strs[position]);
        viewHold.textView.setTextSize(25);
        if (position == 1) {
            startScaleTo(convertView, 0.6f, 1.0f);
        } else {
            startScaleTo(convertView, 1.0f, 0.6f);
        }
        return convertView;
    }

    public void startScaleTo(final View view, float start, float end) {
        ValueAnimator animator = ValueAnimator.ofFloat(start, end);
        animator.setDuration(500);
        animator.start();
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                view.setScaleX(value);
                view.setScaleY(0.4f + (0.6f * value));
            }
        });
    }

    public class ViewHold {
        public TextView textView;
    }
}
