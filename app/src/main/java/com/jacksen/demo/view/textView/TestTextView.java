package com.jacksen.demo.view.textView;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.widget.TextView;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TestTextView extends AppCompatActivity {

    @Bind(R.id.test_textView)
    TextView testTextView;

    @Bind(R.id.link_text_manual)
    TextView linkTextManual;

    @Bind(R.id.link_spannable)
    TextView linkSpannable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_text_view);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        /*SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(testTextView.getText());

        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(TestTextView.this, "clicked", Toast.LENGTH_SHORT).show();
            }
        };
        SpannableString spannableString = new SpannableString("[more]");
        spannableString.setSpan(span, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        spannableStringBuilder.append(spannableString);

//        spannableStringBuilder.setSpan(span, spannableStringBuilder.length() - spannableString.length(), spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        testTextView.setText(spannableStringBuilder);
        testTextView.setHighlightColor(Color.TRANSPARENT);
        testTextView.setMovementMethod(LinkMovementMethod.getInstance());*/

        Toast.makeText(this, "testTextView.getText():" + testTextView.getText(), Toast.LENGTH_SHORT).show();

        linkTextManual = (TextView) findViewById(R.id.link_text_manual);
        linkTextManual.setText(Html.fromHtml(getResources().getString(R.string.link_text_manual)));
        //只调用Html.fromHtml只会在形式上想一个链接，点击并没有效果，还需要下面一行代码
        linkTextManual.setMovementMethod(LinkMovementMethod.getInstance());


        linkSpannable = (TextView) findViewById(R.id.link_spannable);
        SpannableString spannableString = new SpannableString(getResources().getString(R.string.link_spannable_string));
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 2, 5,
                Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new URLSpan("tel:1111110"), 10 + 6, 10 + 10,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        linkSpannable.setText(spannableString);
        linkSpannable.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
