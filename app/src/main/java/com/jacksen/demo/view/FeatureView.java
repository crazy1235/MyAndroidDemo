package com.jacksen.demo.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * Created by ys on 2015/11/10.
 */
public class FeatureView extends FrameLayout {
    public FeatureView(Context context) {
        super(context);

        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.item_detail, this);
    }

    public synchronized void setTitleId(int titleId) {
        ((TextView) (findViewById(R.id.title_item))).setText(titleId);
    }

    public synchronized void setDescriptionId(int descriptionId) {
        ((TextView) (findViewById(R.id.description_item))).setText(descriptionId);
    }

}
