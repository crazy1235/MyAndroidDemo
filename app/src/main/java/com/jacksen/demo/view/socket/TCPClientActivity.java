package com.jacksen.demo.view.socket;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jacksen.demo.view.R;

import java.io.IOException;
import java.net.Socket;

public class TCPClientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tcp_client);
    }

    private void newOneClient(){
        Socket socket = null;
        while(socket == null){
            try {
                socket = new Socket("localhost", 8688);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
