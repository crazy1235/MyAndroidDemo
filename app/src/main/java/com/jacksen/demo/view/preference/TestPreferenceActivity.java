package com.jacksen.demo.view.preference;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.jacksen.demo.view.R;

import java.util.List;

/**
 * test PreferenceActivity
 */
public class TestPreferenceActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_test_preference);

        PreferenceManager.setDefaultValues(this, R.xml.prefer_activity, false);

        addPreferencesFromResource(R.xml.prefer_activity);

        ListPreference sexListPre = (ListPreference) findPreference("sex_list_preference");


    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.getKey().equals("sex_list_preference")) {
            String entry = ((ListPreference) preference).getEntry().toString();
            Toast.makeText(this, entry, Toast.LENGTH_SHORT).show();
        }
        return true;
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }
}
