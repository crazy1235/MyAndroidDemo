package com.jacksen.demo.view.handler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.jacksen.demo.view.R;

/**
 *
 */
public class TestHandler extends AppCompatActivity {

    private TextView testview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_handler);

        testview = (TextView) findViewById(R.id.testview);


        handler.post(new Runnable() {
            @Override
            public void run() {
                testview.setText("post");
            }
        });

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                testview.setText("postDelayed");
            }
        }, 5000);

        handler.postAtTime(new Runnable() {
            @Override
            public void run() {
                testview.setText("postAtTime");
            }
        }, SystemClock.uptimeMillis() + 1000);

        handler.postAtFrontOfQueue(new Runnable() {
            @Override
            public void run() {
                testview.setText("postAtFrontOfQueue");
            }
        });

        handler.postAtTime(new Runnable() {
            @Override
            public void run() {
                testview.setText("postAtTime");
            }
        }, testview, 3000);


        handler.obtainMessage();

//        handler.sendEmptyMessageAtTime()

        new Thread(){
            @Override
            public void run() {
                // 一些耗时的操作

                handler.sendEmptyMessage(1);
            }
        }.start();

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    testview.setText("处理完毕");
                    break;
                default:
                    break;
            }
        }
    };

    private Handler handler2 = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            return false;
        }
    });


    private MyHandler myHanler = new MyHandler();

    class MyHandler extends  Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 1:
                    testview.setText("处理完毕");
                    break;
                default:
                    break;
            }
        }
    }




}
