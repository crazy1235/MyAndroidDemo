package com.jacksen.demo.view.custom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jacksen.demo.view.R;

/**
 * Custom View Demo
 */
public class FirstCustomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_custom_activty);
    }
}
