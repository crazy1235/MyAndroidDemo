package com.jacksen.demo.view.seekbar;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by admin on 2017/11/2.
 */

public class ColorDrawable extends Drawable {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private LinearGradient linearGradient;
    private RectF rectF;

    private @ColorInt
    int[] colors = new int[]{Color.parseColor("#1309C6"), Color.parseColor("#54B7EA")
            , Color.parseColor("#FFF5C9"), Color.parseColor("#FFC432")
            , Color.parseColor("#FF4E4E")};

    private float positions[] = new float[]{0.15f, 0.2f, 0.3f, 0.2f, 0.15f};

    public ColorDrawable() {

    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.drawRoundRect(rectF, 10, 10, paint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        rectF = new RectF(left, top, right, bottom);
        linearGradient = new LinearGradient(0, 0, right, bottom, colors, null
                , Shader.TileMode.CLAMP);
        paint.setShader(linearGradient);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
