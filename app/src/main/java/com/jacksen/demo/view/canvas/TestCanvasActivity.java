package com.jacksen.demo.view.canvas;

import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.jacksen.demo.view.R;

public class TestCanvasActivity extends AppCompatActivity {

    private MyCanvasView myCanvasView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_canvas);

        myCanvasView = (MyCanvasView) findViewById(R.id.my_canvas_view);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_xfermode, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                myCanvasView.changeXfermode(PorterDuff.Mode.CLEAR);
                break;
            case R.id.action_src:
                myCanvasView.changeXfermode(PorterDuff.Mode.SRC);
                break;
            case R.id.action_dst:
                myCanvasView.changeXfermode(PorterDuff.Mode.DST);
                break;
            case R.id.action_src_over:
                myCanvasView.changeXfermode(PorterDuff.Mode.SRC_OVER);
                break;
            case R.id.action_dst_over:
                myCanvasView.changeXfermode(PorterDuff.Mode.DST_OVER);
                break;
            case R.id.action_src_in:
                myCanvasView.changeXfermode(PorterDuff.Mode.SRC_IN);
                break;
            case R.id.action_dst_in:
                myCanvasView.changeXfermode(PorterDuff.Mode.DST_IN);
                break;
            case R.id.action_src_out:
                myCanvasView.changeXfermode(PorterDuff.Mode.SRC_OUT);
                break;
            case R.id.action_dst_out:
                myCanvasView.changeXfermode(PorterDuff.Mode.DST_OUT);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
