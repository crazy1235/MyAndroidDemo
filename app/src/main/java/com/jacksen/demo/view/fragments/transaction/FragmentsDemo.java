package com.jacksen.demo.view.fragments.transaction;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.jacksen.demo.view.R;

/**
 * 测试fragment的 hide show 方法
 *
 * @author jacksen
 */
public class FragmentsDemo extends AppCompatActivity {


    private FrameLayout contentLayout;

    private Button hideFragmentBtn;

    private Button showFragmentBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments_demo);


        contentLayout = (FrameLayout) findViewById(R.id.content_layout);

        hideFragmentBtn = (Button) findViewById(R.id.hide_fragment_btn);

        showFragmentBtn = (Button) findViewById(R.id.show_fragment_btn);

        final TabFragment4 fragment4 = TabFragment4.newInstance();
        final TabFragment5 fragment5 = TabFragment5.newInstance();

        final FragmentManager fragmentManager = getSupportFragmentManager();

//        fragmentManager.beginTransaction().add(R.id.content_layout, fragment5).commit();
//        fragmentManager.beginTransaction().add(R.id.content_layout, fragment4).addToBackStack("fragment4").commit();

        fragmentManager.beginTransaction().show(fragment4).commit();

//        fragmentManager.beginTransaction().replace(R.id.content_layout, fragment1).commit();

//        fragmentManager.beginTransaction().hide(fragment1).commit();
//        fragmentManager.beginTransaction().show(fragment2).commit();

//        fragment1.setBackground("#E0EEE0");

        hideFragmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().hide(fragment4).commit();
            }
        });


        showFragmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().show(fragment4).commit();
                fragment4.setBackground("#B8860B");
            }
        });

    }
}
