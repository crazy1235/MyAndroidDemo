package com.jacksen.demo.view.listview;

import java.util.List;

/**
 * Created by jacksen on 2015/12/18.
 */
public class GroupBean {

    private String name;

    private List<ChildrenBean> childList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChildrenBean> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildrenBean> childList) {
        this.childList = childList;
    }
}
