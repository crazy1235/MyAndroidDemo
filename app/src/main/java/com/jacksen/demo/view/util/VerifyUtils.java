package com.jacksen.demo.view.util;

import java.util.regex.Pattern;

/**
 * Created by jacksen on 2016/1/13.
 */
public class VerifyUtils {

    /**
     * 验证邮箱
     *
     * @param email
     * @return
     */
    public static boolean verifyEmail(String email) {
        return Pattern.matches("^\\w+@[0-9a-zA-Z]+\\.[0-9a-zA-Z]+$", email);
    }


    /**
     * 验证手机号
     *
     * @param phoneNumber
     * @return
     */
    public static boolean verifyPhone(String phoneNumber) {
        return Pattern.matches("^(?:\\+?86)?\\s+?(1[34578]\\d{9})$", phoneNumber);
    }

    /**
     * 验证身份证号
     *
     * @param idCard
     * @return
     */
    public static boolean verifyIDCard(String idCard) {
        return Pattern.matches("^\\d{6}(?:1|2){1}\\d{3}(0[1-9]|1[0-2])[0-3][0-9](\\d{4}|\\d{3}X)$", idCard);
    }

    /**
     * 验证密码 (字母开头，只能包括字母、数字、下划线，长度6-18)
     *
     * @param password
     * @return
     */
    public static boolean verifyPassword(String password) {
        return Pattern.matches("[A-Za-z]\\w{5,17}", password);
    }


    /**
     *
     * @param password
     * @return
     */
    /*public static boolean strictVerifyPassword(String password) {
        return Pattern.matches("", password);
    }*/

}
