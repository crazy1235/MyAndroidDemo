package com.jacksen.demo.view.surface

import javax.microedition.khronos.opengles.GL10
import android.opengl.GLUtils
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.R.attr.order
import android.content.Context
import com.jacksen.demo.view.R
import java.nio.ByteBuffer
import java.nio.ByteBuffer.allocateDirect
import java.nio.ByteOrder
import java.nio.FloatBuffer


/**
 * Created by admin on 2017/12/23.
 */
class GLBitmap {

    private var textureBuffer: FloatBuffer // buffer holding the texture coordinates
    private val texture = floatArrayOf(
            // Mapping coordinates for the vertices
            0.0f, 1.0f, // top left (V2)
            0.0f, 0.0f, // bottom left (V1)
            1.0f, 1.0f, // top right (V4)
            1.0f, 0.0f // bottom right (V3)
    )

    private var vertexBuffer: FloatBuffer // buffer holding the vertices

    private val vertices = floatArrayOf(-1.0f, -1.0f, 0.0f, // V1 - bottom left
            -1.0f, 1.0f, 0.0f, // V2 - top left
            1.0f, -1.0f, 0.0f, // V3 - bottom right
            1.0f, 1.0f, 0.0f // V4 - top right
    )

    init {
        var byteBuffer = ByteBuffer.allocateDirect(vertices.size * 4)
        byteBuffer.order(ByteOrder.nativeOrder())
        vertexBuffer = byteBuffer.asFloatBuffer()
        vertexBuffer.put(vertices)
        vertexBuffer.position(0)

        byteBuffer = ByteBuffer.allocateDirect(texture.size * 4)
        byteBuffer.order(ByteOrder.nativeOrder())
        textureBuffer = byteBuffer.asFloatBuffer()
        textureBuffer.put(texture)
        textureBuffer.position(0)
    }

    /** The texture pointer  */
    private val textures = IntArray(1)

    fun loadGLTexture(gl: GL10, context: Context) {
        // loading texture
        val bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.child)

        // generate one texture pointer
        gl.glGenTextures(1, textures, 0)
        // ...and bind it to our array
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0])

        // create nearest filtered texture
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
                GL10.GL_NEAREST.toFloat())
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
                GL10.GL_LINEAR.toFloat())

        // Use Android GLUtils to specify a two-dimensional texture image from
        // our bitmap
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0)

        // Clean up
        bitmap.recycle()
    }

    fun draw(gl: GL10) {
        // bind the previously generated texture

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0])

        // Point to our buffers
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY)
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY)

        // Set the face rotation
        gl.glFrontFace(GL10.GL_CW)

        // Point to our vertex buffer
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer)
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer)

        // Draw the vertices as triangle strip
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.size / 3)

        // Disable the client state before leaving
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY)
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY)
    }
}