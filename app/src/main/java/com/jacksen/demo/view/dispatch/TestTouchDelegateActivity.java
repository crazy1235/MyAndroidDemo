package com.jacksen.demo.view.dispatch;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jacksen.demo.view.R;

public class TestTouchDelegateActivity extends AppCompatActivity {

    private RelativeLayout root_layout;
    private Button touch_delegate_btn;
    private InteractiveLineGraphView mGraphView;
    private View area_indicator;
    private int offset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_touch_delegate);

        area_indicator = findViewById(R.id.area_indicator);
        root_layout = (RelativeLayout) findViewById(R.id.root_layout);
        touch_delegate_btn = (Button) findViewById(R.id.touch_delegate_btn);
        mGraphView = (InteractiveLineGraphView) findViewById(R.id.chart);

        offset = (int) (getResources().getDisplayMetrics().density * 50 + 0.5f);

        root_layout.post(new Runnable() {
            @Override
            public void run() {
                Rect bound = new Rect();
                touch_delegate_btn.getHitRect(bound);

                bound.left -= offset;
                bound.top -= offset;
                bound.right += offset;
                bound.bottom += offset;

                TouchDelegate touchDelegate = new TouchDelegate(bound, touch_delegate_btn);
                if (View.class.isInstance(touch_delegate_btn.getParent())) {
                    ((View) touch_delegate_btn.getParent()).setTouchDelegate(touchDelegate);
                }

                ViewGroup.LayoutParams params = area_indicator.getLayoutParams();

                params.width = bound.width();
                params.height = bound.height();

                area_indicator.setLayoutParams(params);

            }
        });

        touch_delegate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(TestTouchDelegateActivity.this, "button has been clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_touch_delegate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_zoom_in:
                mGraphView.zoomIn();
                return true;

            case R.id.action_zoom_out:
                mGraphView.zoomOut();
                return true;

            case R.id.action_pan_left:
                mGraphView.panLeft();
                return true;

            case R.id.action_pan_right:
                mGraphView.panRight();
                return true;

            case R.id.action_pan_up:
                mGraphView.panUp();
                return true;

            case R.id.action_pan_down:
                mGraphView.panDown();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
