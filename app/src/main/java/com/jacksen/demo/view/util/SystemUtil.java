package com.jacksen.demo.view.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by Admin on 2017/5/26.
 */

public class SystemUtil {

    private static final DisplayMetrics sMetrics = Resources.getSystem().getDisplayMetrics();

    public static int getScreenWidth() {
        return sMetrics != null ? sMetrics.widthPixels : 0;
    }

    public static int getScreenHeight() {
        return sMetrics != null ? sMetrics.heightPixels : 0;
    }

    public static int dp2px(Context context, float dpValue) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, context.getResources().getDisplayMetrics());
    }

    public static int dp2px(float dipValue) {
        float scale = sMetrics != null?sMetrics.density:1.0F;
        return (int)(dipValue * scale + 0.5F);
    }

    public static int sp2px(float spValue) {
        float fontScale = sMetrics != null?sMetrics.scaledDensity:1.0F;
        return (int)(spValue * fontScale + 0.5F);
    }

    public static float sp2px(Context context, float spValue) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, context.getResources().getDisplayMetrics());
    }
}
