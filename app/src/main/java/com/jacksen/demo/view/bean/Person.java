package com.jacksen.demo.view.bean;

/**
 * @author ys
 *         Created by Admin on 2015/9/20.
 */
public class Person {


    /**
     * name : jacksen
     * height : 175.5
     * age : 23
     */

    private String name;
    private double height;
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public double getHeight() {
        return height;
    }

    public int getAge() {
        return age;
    }
}
