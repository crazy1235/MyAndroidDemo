package com.jacksen.demo.view.recyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jacksen.demo.view.R;
import com.jacksen.demo.view.recyclerView.touchhelper.ItemTouchHelperListener;

import java.util.Collections;
import java.util.List;

/**
 * Created by Admin on 2015/9/5.
 */
public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerAdapter1.ViewHolder> implements ItemTouchHelperListener {

    private List<String> dataSet;

    public RecyclerAdapter1(List<String> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_test_recycler, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getTextView().setText(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(dataSet, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        dataSet.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView getTextView() {
            return textView;
        }

        private TextView textView;

        public ViewHolder(final View itemView) {
            super(itemView);

            textView = (TextView) itemView.findViewById(R.id.textview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), getAdapterPosition() + "\n" + getTextView().getText(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
