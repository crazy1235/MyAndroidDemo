package com.jacksen.demo.view.surface

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.jacksen.demo.view.R
import kotlinx.android.synthetic.main.activity_test_gl_surface_view.*

class TestGlSurfaceView : AppCompatActivity() {

    private val cameraDisplay by lazy { CameraDisplay(glSurfaceView) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("TestGlSurfaceView", "onCreate -- ")
        setContentView(R.layout.activity_test_gl_surface_view)

        renderBtn.setOnClickListener {
            cameraDisplay.render()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("TestGlSurfaceView", "onResume -- ")
        cameraDisplay.onResume()
    }

    override fun onPause() {
        super.onPause()
        Log.d("TestGlSurfaceView", "onPause -- ")
        cameraDisplay.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_glsurfaceview, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.triangleItem -> {

            }
            R.id.squareItem -> {

            }
        }
        return true
    }

}
