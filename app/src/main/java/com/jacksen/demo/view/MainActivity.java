package com.jacksen.demo.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.jacksen.demo.view.actionBar.TestActionBar;
import com.jacksen.demo.view.alertdialog.TestAlertDialogActivity;
import com.jacksen.demo.view.anim.TestAnimActivity;
import com.jacksen.demo.view.anim.TestAnimation;
import com.jacksen.demo.view.anim.TestPropertyAnimation;
import com.jacksen.demo.view.anim.TestSceneAnimation;
import com.jacksen.demo.view.anim.TestTransitionActivity;
import com.jacksen.demo.view.auto.RecordActivity;
import com.jacksen.demo.view.auto.TestAutoCompleteTextView;
import com.jacksen.demo.view.canvas.TestCanvasActivity;
import com.jacksen.demo.view.cardView.TestCardView;
import com.jacksen.demo.view.circularReveal.CircularRevealDemo;
import com.jacksen.demo.view.contacts.ReadContactActivity;
import com.jacksen.demo.view.contentProvider.TestProviderActivity;
import com.jacksen.demo.view.contextmenu.TestContextMenu;
import com.jacksen.demo.view.custom.FirstCustomActivity;
import com.jacksen.demo.view.dispatch.TestDispatchActivity;
import com.jacksen.demo.view.dispatch.TestSlideConflictActivity;
import com.jacksen.demo.view.dispatch.TestTouchDelegateActivity;
import com.jacksen.demo.view.drawerview.NavigationDrawerViewDemo;
import com.jacksen.demo.view.editText.TestEditTextActivity;
import com.jacksen.demo.view.editText.TextInputLayoutDemo;
import com.jacksen.demo.view.egl.TestEGLActivity;
import com.jacksen.demo.view.fragments.ArticleActivity;
import com.jacksen.demo.view.fragments.TabActivity;
import com.jacksen.demo.view.fragments.transaction.FragmentsDemo;
import com.jacksen.demo.view.fragments.transaction.TestFragments;
import com.jacksen.demo.view.gesture.TestGesture;
import com.jacksen.demo.view.gridview.TestGridViewActivity;
import com.jacksen.demo.view.handler.TestHandler;
import com.jacksen.demo.view.immersionStatusBar.ImmersionDemo1;
import com.jacksen.demo.view.kotlin.TestCoroutinesActivity;
import com.jacksen.demo.view.launchmode.AActivity;
import com.jacksen.demo.view.layoutinflater.TestLayoutInflaterActivity;
import com.jacksen.demo.view.listview.TestClipChildrenActivity;
import com.jacksen.demo.view.listview.TestExpandableListView;
import com.jacksen.demo.view.listview.simple.TestItemZoom;
import com.jacksen.demo.view.menu.TestMenuActivity;
import com.jacksen.demo.view.messenger.TestMessengerActivity;
import com.jacksen.demo.view.nougat.FileActivity;
import com.jacksen.demo.view.numberFormat.TestNumberFormat;
import com.jacksen.demo.view.optimization.OptimizationActivity;
import com.jacksen.demo.view.parcelable.Parce1Activity;
import com.jacksen.demo.view.permission.TestPermission;
import com.jacksen.demo.view.progress.TestProgressActivity;
import com.jacksen.demo.view.progressbar.TestProgressbar;
import com.jacksen.demo.view.recyclerView.TestRecyclerView;
import com.jacksen.demo.view.recyclerView.TestRecyclerView2;
import com.jacksen.demo.view.recyclerView.TestRecyclerView4;
import com.jacksen.demo.view.recyclerView.snaphelper.TestSnapHelper;
import com.jacksen.demo.view.rxjava.TestRxJavaActivity;
import com.jacksen.demo.view.searchview.TestSearchView;
import com.jacksen.demo.view.seekbar.TestSeekBarActivity;
import com.jacksen.demo.view.selector.TestSelectorActivity;
import com.jacksen.demo.view.services.TestServiceActivity;
import com.jacksen.demo.view.slideDelete.SwipeDismissBehaviorDemo;
import com.jacksen.demo.view.spannableString.TestSpannableString;
import com.jacksen.demo.view.spinner.TestSpinnerActivity;
import com.jacksen.demo.view.surface.SurfaceViewTest;
import com.jacksen.demo.view.surface.TestGlSurfaceView;
import com.jacksen.demo.view.systemui.TestSystemUIActivity;
import com.jacksen.demo.view.taggroup.TestTagGroupActivity;
import com.jacksen.demo.view.textView.TestTextView;
import com.jacksen.demo.view.time.TestChronometer;
import com.jacksen.demo.view.toolbar.TestToolBar;
import com.jacksen.demo.view.video.TestVideoView;
import com.jacksen.demo.view.view.TestViewActivity;
import com.jacksen.demo.view.viewAnimator.TestTextSwitcher;
import com.jacksen.demo.view.viewpager.TestViewPager;
import com.jacksen.demo.view.webview.TestAndroidAndJs;
import com.jacksen.demo.view.webview.TestChromeCustomTabs;
import com.jacksen.demo.view.widget.TestWidgetActivity;
import com.jacksen.demo.view.window.TestWindowActivity;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author ys
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    @Bind(R.id.listView)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Log.d("MainActivity", "onCreate");
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    /**
     *
     */
    private void init() {
        List data = Arrays.asList(DETAILS);
        Collections.reverse(data);
        ListAdapter adapter = new CustomArrayAdapter(this, data);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *
     */
    /*private void addBadge() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentText("title").
                setContentText("text").
                setSmallIcon(R.mipmap.ic_launcher);
        Notification notification = builder.build();
        try {
            Field field = notification.getClass().getDeclaredField("extraNotification");
            Object extraNotification = field.get(notification);
            Method method = extraNotification.getClass().getDeclaredMethod("setMessageCount", int.class);
            method.invoke(extraNotification, 12);
        } catch (Exception e) {
            e.printStackTrace();
        }
        notificationManager.notify(1, notification);
    }*/

    /**
     *
     */
    private static final ActivityDetails[] DETAILS = {
            new ActivityDetails(R.string.test_recycler_view, R.string.no_description, TestRecyclerView.class),
            new ActivityDetails(R.string.test_recycler_view2, R.string.no_description, TestRecyclerView2.class),
            new ActivityDetails(R.string.test_card_view, R.string.no_description, TestCardView.class),
            new ActivityDetails(R.string.test_chrome_custom_tabs, R.string.no_description, TestChromeCustomTabs.class),
            new ActivityDetails(R.string.test_number_format, R.string.no_description, TestNumberFormat.class),
            new ActivityDetails(R.string.test_seek_bar, R.string.no_description, TestSeekBarActivity.class),
            new ActivityDetails(R.string.test_service, R.string.no_description, TestServiceActivity.class),
            new ActivityDetails(R.string.test_action_bar, R.string.no_description, TestActionBar.class),
            new ActivityDetails(R.string.test_edit_text, R.string.no_description, TestEditTextActivity.class),
            new ActivityDetails(R.string.test_read_contact, R.string.no_description, ReadContactActivity.class),
            new ActivityDetails(R.string.test_slide_delete_btn, R.string.no_description, SwipeDismissBehaviorDemo.class),
            new ActivityDetails(R.string.test_TextInputLayout, R.string.no_description, TextInputLayoutDemo.class),
            new ActivityDetails(R.string.test_immersion, R.string.no_description, ImmersionDemo1.class),
            new ActivityDetails(R.string.test_immersion, R.string.no_description, ImmersionDemo1.class),
            new ActivityDetails(R.string.test_chronometer, R.string.no_description, TestChronometer.class),
            new ActivityDetails(R.string.test_autoCompleteTextView, R.string.no_description, TestAutoCompleteTextView.class),
            new ActivityDetails(R.string.test_expandable_list_view, R.string.no_description, TestExpandableListView.class),
            new ActivityDetails(R.string.test_spannableStringBuilder, R.string.no_description, TestSpannableString.class),
            new ActivityDetails(R.string.test_anim, R.string.no_description, TestAnimActivity.class),
            new ActivityDetails(R.string.test_handler, R.string.no_description, TestHandler.class),
            new ActivityDetails(R.string.article_activity, R.string.no_description, ArticleActivity.class),
            new ActivityDetails(R.string.test_tabs_activity, R.string.no_description, TabActivity.class),
            new ActivityDetails(R.string.test_fragments, R.string.no_description, TestFragments.class),
            new ActivityDetails(R.string.test_textView, R.string.no_description, TestTextView.class),
            new ActivityDetails(R.string.test_video_view, R.string.no_description, TestVideoView.class),
            new ActivityDetails(R.string.test_gesture, R.string.no_description, TestGesture.class),
            new ActivityDetails(R.string.test_toolbar, R.string.no_description, TestToolBar.class),
            new ActivityDetails(R.string.test_animation, R.string.no_description, TestAnimation.class),
            new ActivityDetails(R.string.test_gridView, R.string.no_description, TestGridViewActivity.class),
            new ActivityDetails(R.string.test_GridRecyclerView, R.string.no_description, TestRecyclerView4.class),
            new ActivityDetails(R.string.test_menu, R.string.no_description, TestMenuActivity.class),
            new ActivityDetails(R.string.test_view_pager, R.string.no_description, TestViewPager.class),
            new ActivityDetails(R.string.test_property_animation, R.string.no_description, TestPropertyAnimation.class),
            new ActivityDetails(R.string.test_navigation_drawer_view_demo, R.string.no_description, NavigationDrawerViewDemo.class),
            new ActivityDetails(R.string.test_circular_reveal, R.string.no_description, CircularRevealDemo.class),
            new ActivityDetails(R.string.test_scene_anim, R.string.no_description, TestSceneAnimation.class),
            new ActivityDetails(R.string.test_permission, R.string.no_description, TestPermission.class),
            new ActivityDetails(R.string.test_item_zoom_lv, R.string.no_description, TestItemZoom.class),
            new ActivityDetails(R.string.test_searchview, R.string.no_description, TestSearchView.class),
            new ActivityDetails(R.string.test_text_switcher, R.string.no_description, TestTextSwitcher.class),
            new ActivityDetails(R.string.test_widget, R.string.no_description, TestWidgetActivity.class),
            new ActivityDetails(R.string.test_transition, R.string.no_description, TestTransitionActivity.class),
            new ActivityDetails(R.string.test_parcelable, R.string.no_description, Parce1Activity.class),
            new ActivityDetails(R.string.test_file, R.string.no_description, FileActivity.class),
            new ActivityDetails(R.string.test_android_js, R.string.no_description, TestAndroidAndJs.class),
            new ActivityDetails(R.string.test_record_history, R.string.no_description, RecordActivity.class),
            new ActivityDetails(R.string.test_progressbar, R.string.no_description, TestProgressbar.class),
            new ActivityDetails(R.string.test_map, R.string.no_description, OptimizationActivity.class),
            new ActivityDetails(R.string.test_fragment_hide_show, R.string.no_description, FragmentsDemo.class),
            new ActivityDetails(R.string.test_snap_helper, R.string.no_description, TestSnapHelper.class),
            new ActivityDetails(R.string.test_context_menu, R.string.no_description, TestContextMenu.class),
            new ActivityDetails(R.string.test_selector, R.string.no_description, TestSelectorActivity.class),
            new ActivityDetails(R.string.test_spinner, R.string.no_description, TestSpinnerActivity.class),
            new ActivityDetails(R.string.test_alert_dialog, R.string.no_description, TestAlertDialogActivity.class),
            new ActivityDetails(R.string.test_layout_inflater, R.string.no_description, TestLayoutInflaterActivity.class),
            new ActivityDetails(R.string.test_dispatch, R.string.no_description, TestDispatchActivity.class),
            new ActivityDetails(R.string.test_touch_delegate, R.string.no_description, TestTouchDelegateActivity.class),
            new ActivityDetails(R.string.test_slide_conflict, R.string.no_description, TestSlideConflictActivity.class),
            new ActivityDetails(R.string.test_first_custom, R.string.no_description, FirstCustomActivity.class),
            new ActivityDetails(R.string.test_view, R.string.no_description, TestViewActivity.class),
            new ActivityDetails(R.string.test_messenger, R.string.no_description, TestMessengerActivity.class),
            new ActivityDetails(R.string.test_content_provider, R.string.no_description, TestProviderActivity.class),
            new ActivityDetails(R.string.test_window, R.string.no_description, TestWindowActivity.class),
            new ActivityDetails(R.string.test_clip_children, R.string.no_description, TestClipChildrenActivity.class),
            new ActivityDetails(R.string.test_progress, R.string.no_description, TestProgressActivity.class),
            new ActivityDetails(R.string.test_canvas, R.string.no_description, TestCanvasActivity.class),
            new ActivityDetails(R.string.test_tag_group, R.string.no_description, TestTagGroupActivity.class),
            new ActivityDetails(R.string.test_rxjava, R.string.no_description, TestRxJavaActivity.class),
            new ActivityDetails(R.string.test_launch_mode, R.string.no_description, AActivity.class),
            new ActivityDetails(R.string.test_system_ui, R.string.no_description, TestSystemUIActivity.class),
            new ActivityDetails(R.string.test_surface_view, R.string.no_description, SurfaceViewTest.class),
            new ActivityDetails(R.string.test_gl_surface_view, R.string.no_description, TestGlSurfaceView.class),
            new ActivityDetails(R.string.test_coroutines, R.string.no_description, TestCoroutinesActivity.class),
            new ActivityDetails(R.string.test_egl_surface_view, R.string.no_description, TestEGLActivity.class)
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ActivityDetails details = DETAILS[position];
        startActivity(new Intent(this, details.activityClass));
    }

    /**
     *
     */
    private static class CustomArrayAdapter extends ArrayAdapter<ActivityDetails> {

        public CustomArrayAdapter(Context context, List<ActivityDetails> details) {
            super(context, R.layout.item_detail, R.id.title_item, details);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FeatureView featureView;
            if (convertView instanceof FeatureView) {
                featureView = (FeatureView) convertView;
            } else {
                featureView = new FeatureView(getContext());
            }

            ActivityDetails detail = getItem(position);
            featureView.setTitleId(detail.titleId);
            featureView.setDescriptionId(detail.descriptionId);
            return featureView;
        }
    }

    /**
     * 静态内部类
     */
    private static class ActivityDetails {
        private int titleId;
        private int descriptionId;
        private Class<? extends AppCompatActivity> activityClass;

        public ActivityDetails(int titleId, int descriptionId, Class<? extends AppCompatActivity> activityClass) {
            super();
            this.titleId = titleId;
            this.descriptionId = descriptionId;
            this.activityClass = activityClass;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
    }
}
