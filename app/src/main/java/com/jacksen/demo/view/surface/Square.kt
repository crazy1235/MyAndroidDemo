package com.jacksen.demo.view.surface

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

class Square {

    private var vertexBuffer: FloatBuffer
    private var drawOrderBuffer: ShortBuffer

    companion object {

        const val COORDS_PER_VERTEX = 3

        var squareCoords = floatArrayOf(
                -0.5f, 0.5f, 0.0f, // top left
                -0.5f, -0.5f, 0.0f, // bottom left
                0.5f, -0.5f, 0.0f, // bottom right
                0.5f, 0.5f, 0.0f) // top right

        // 绘制顶点顺序
        val drawOrder = shortArrayOf(0, 1, 2, 0, 2, 3)
    }

    init {
        val byteBuffer = ByteBuffer.allocateDirect(squareCoords.size * 4)
        byteBuffer.order(ByteOrder.nativeOrder())
        vertexBuffer = byteBuffer.asFloatBuffer()
        vertexBuffer.put(squareCoords)
        vertexBuffer.position(0)

        val orderByteBuffer = ByteBuffer.allocateDirect(drawOrder.size * 2)
        orderByteBuffer.order(ByteOrder.nativeOrder())
        drawOrderBuffer = orderByteBuffer.asShortBuffer()
        drawOrderBuffer.put(drawOrder)
        drawOrderBuffer.position(0)
    }
}