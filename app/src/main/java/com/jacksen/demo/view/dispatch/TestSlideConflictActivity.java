package com.jacksen.demo.view.dispatch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jacksen.demo.view.R;

import java.util.ArrayList;
import java.util.List;

public class TestSlideConflictActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_slide_conflict);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        List<String> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add("item : " + i);
        }

        recyclerView.setAdapter(new SlideConflictAdapter(list, this));


    }

}
