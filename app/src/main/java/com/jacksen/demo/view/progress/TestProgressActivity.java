package com.jacksen.demo.view.progress;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;

        public class TestProgressActivity extends AppCompatActivity {


            @Bind(R.id.show_btn)
            Button showBtn;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_test_progress);
                ButterKnife.bind(this);

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog dialog4 = ProgressDialog.show(TestProgressActivity.this, "提示", "正在登陆中",
                        false, true);

            }
        });
    }
}
