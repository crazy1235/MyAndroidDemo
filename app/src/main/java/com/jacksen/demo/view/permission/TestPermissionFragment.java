package com.jacksen.demo.view.permission;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jacksen.demo.view.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 */
public class TestPermissionFragment extends Fragment {

    @Bind(R.id.makeCallBtn)
    Button makeCallBtn;

    public TestPermissionFragment() {
        // Required empty public constructor
    }

    /**
     */
    public static TestPermissionFragment newInstance(String param1, String param2) {
        TestPermissionFragment fragment = new TestPermissionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_permission, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.makeCallBtn)
    void onCallBtnClick(View view) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri uri = Uri.parse("tel:" + "10086");
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
