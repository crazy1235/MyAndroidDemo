package com.jacksen.demo.view.webview;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.v7.app.AppCompatActivity;

import com.jacksen.demo.view.R;

/**
 * @author ys
 *         测试Chrome Custom Tabs
 */
public class TestChromeCustomTabs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_chrome_custom_tabs);

        init();

    }

    private void init() {
        String packageName = "com.android.chrome";
        CustomTabsServiceConnection customTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        CustomTabsClient.bindCustomTabsService(this, packageName, customTabsServiceConnection);

        //
//        Uri uri = Uri.parse("http://developer.android.com");

//        CustomTabsSession customTabsSession = new CustomTabsSession();
//        CustomTabsIntent customTabsIntent = CustomTabsIntent.Builder().builder();
//        customTabsIntent.launchUrl(this, uri);
    }


    /**
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, TestChromeCustomTabs.class));
    }

}
