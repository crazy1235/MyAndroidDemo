package com.jacksen.demo.view.accessibility;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

/**
 * Accessibility Service
 * <p/>
 * Created by jacksen on 2016/2/3.
 */
public class MyAccessibilityService extends AccessibilityService {

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d("MyAccessibilityService", "onServiceConnected -- 成功连接服务");
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        int eventType = event.getEventType();
        switch (eventType){
            case AccessibilityEvent.TYPE_VIEW_CLICKED:
                Log.d("MyAccessibilityService", "clicked");
                break;
            case AccessibilityEvent.TYPE_VIEW_FOCUSED:
                Log.d("MyAccessibilityService", "focused");
                break;
            case AccessibilityEvent.TYPE_VIEW_SCROLLED:
                Log.d("MyAccessibilityService", "scrolled");
                break;
        }
        Log.d("MyAccessibilityService", "eventType:" + eventType);
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("MyAccessibilityService", "onUnbind");
        return super.onUnbind(intent);
    }

}
