// IDownloadService.aidl
package com.jacksen.demo.view.aidl;
import com.jacksen.demo.view.aidl.TestAidlBean;
import com.jacksen.demo.view.aidl.IDownloadListener;
// Declare any non-default types here with import statements

interface IDownloadService {


    /**
     * Request the process ID of this service, to do evil things whith it.
     *
     */
    int getPid();

    TestAidlBean getAidlBean(String name);

    void startDownload(in String url);

    void registerListener(IDownloadListener listener);
    void unregisterListener(IDownloadListener listener);
}
