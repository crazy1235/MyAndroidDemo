// IDownloadListener.aidl
package com.jacksen.demo.view.aidl;

// Declare any non-default types here with import statements

interface IDownloadListener {

    //下载中断
    void onDownloadInterrupt(int progress);

    void onDownloadProgress(int progress);
}
